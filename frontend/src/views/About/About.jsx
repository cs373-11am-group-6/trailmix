import React, { useState, useEffect } from "react";
import styles from "./About.module.css";
import { teamMembers } from "./Info";
import { Row, Col, Card, Container } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const getInfo = async () => {
  let commitCount = 0,
    issueCount = 0,
    testCount = 0;

  teamMembers.forEach((member) => {
    member.Commits = 0;
    member.Issues = 0;
    testCount += member.Tests;
  });

  let pageNum = 1;
  let page = [];
  let commits = [];
  do {
    page = await fetch(
      `https://gitlab.com/api/v4/projects/29922526/repository/commits?per_page=100&page=${pageNum}&all=true`
    );
    page = await page.json();
    commits = [...commits, ...page];
    pageNum += 1;
  } while (page.length === 100);

  commits.forEach((commit) => {
    const { author_name, author_email } = commit;
    teamMembers.forEach((member) => {
      if (
        member.Name === author_name ||
        member.Username === author_name ||
        member.Email === author_email
      ) {
        member.Commits += 1;
      }
    });
    commitCount += 1;
  });

  pageNum = 0;
  page = [];
  let issues = [];
  do {
    page = await fetch(
      `https://gitlab.com/api/v4/projects/29922526/issues?per_page=100&page=${pageNum}`
    );
    page = await page.json();
    issues = [...issues, ...page];
    pageNum += 1;
  } while (page.length === 100);

  issues.forEach((issue) => {
    const { assignees } = issue;
    assignees.forEach((assignee) => {
      const { name } = assignee;
      teamMembers.forEach((member) => {
        if (member.Name === name || member.Username === name) {
          member.Issues += 1;
        }
      });
    });
    issueCount += 1;
  });

  return {
    totalCommits: commitCount,
    totalIssues: issueCount,
    totalTests: testCount,
    teamMembers: teamMembers,
  };
};

const About = () => {
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);
  const [totalTests, setTotalTests] = useState(0);
  const [teamMems, setTeamMems] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchInfo = async () => {
      if (teamMems === undefined || teamMems.length === 0) {
        const info = await getInfo();
        setTotalCommits(info.totalCommits);
        setTotalIssues(info.totalIssues);
        setTotalTests(info.totalTests);
        setTeamMems(info.teamMembers);
      }
      setLoading(false);
    };
    fetchInfo();
  }, [teamMems]);
  return (
    <div className={styles.aboutPage}>
      <Container className={styles.aboutPage}>
        <div className={styles.content}>
          <div className={styles.description}>
            <h1>About Us</h1>
            <p className={styles.descParagraph}>
              TrailMix compiles information on US National Parks, park hiking
              trails, and nearby lodging options for the convenience of our
              users. By making this information easily accessible, TrailMix
              hopes to encourage more people to visit our national parks, engage
              with the outdoors, and support the continued preservation of our
              environment.
            </p>
          </div>
          <div className={styles.members}>
            <h1>Team Members</h1>
            {loading ? (
              <h2>
                <FontAwesomeIcon icon={faSpinner} spin />
              </h2>
            ) : (
              <>
                <Row xs={1} sm={1} md={3}>
                  {teamMems.map((member) => {
                    return (
                      <Col key={member.Name} as="div">
                        <Card>
                          <Card.Title>{member.Name}</Card.Title>
                          <Card.Img variant="top" src={member.Photo} />
                          <Card.Subtitle>{member.Role}</Card.Subtitle>
                          <Card.Text>
                            {member.Bio}
                            <p></p>
                            Commits: {member.Commits}, Issues: {member.Issues},
                            Tests: {member.Tests}
                          </Card.Text>
                        </Card>
                      </Col>
                    );
                  })}
                </Row>
              </>
            )}
          </div>
          <div className={styles.totalInfo}>
            <h1>Total Repository Info</h1>
            <Card>
              <Card.Text>
                Commits:{" "}
                {loading ? (
                  <FontAwesomeIcon icon={faSpinner} spin />
                ) : (
                  totalCommits
                )}
              </Card.Text>
            </Card>
            <Card>
              <Card.Text>
                Issues:{" "}
                {loading ? (
                  <FontAwesomeIcon icon={faSpinner} spin />
                ) : (
                  totalIssues
                )}
              </Card.Text>
            </Card>
            <Card>
              <Card.Text>
                Tests:{" "}
                {loading ? (
                  <FontAwesomeIcon icon={faSpinner} spin />
                ) : (
                  totalTests
                )}
              </Card.Text>
            </Card>
          </div>
          <div className={styles.dataSources}>
            <h1>API Sources</h1>
            <Card
              as="a"
              href="https://www.nps.gov/subjects/developer/api-documentation.htm"
              target="_blank"
            >
              <Card.Title>National Park Service API</Card.Title>
              <Card.Text>Scraped for Park and Trail Info</Card.Text>
            </Card>
            <Card
              as="a"
              href="https://ridb.recreation.gov/docs"
              target="_blank"
            >
              <Card.Title>Recreation.gov API</Card.Title>
              <Card.Text>Scraped for Lodging Info</Card.Text>
            </Card>
            <Card
              as="a"
              href="https://developers.google.com/maps/documentation"
              target="_blank"
            >
              <Card.Title>Google Maps API</Card.Title>
              <Card.Text>Goecoding, Maps</Card.Text>
            </Card>
          </div>
          <div className={styles.tools}>
            <h1>Development Tools</h1>
            <Card as="a" href="https://reactjs.org/" target="_blank">
              <Card.Title>React</Card.Title>
              <Card.Text>Javascript Library</Card.Text>
            </Card>
            <Card
              as="a"
              href="https://gitlab.com/cs373-11am-group-6/trailmix"
              target="_blank"
            >
              <Card.Title>GitLab</Card.Title>
              <Card.Text>Git Repository + CI/CD Workflow</Card.Text>
            </Card>
            <Card as="a" href="https://prettier.io/" target="_blank">
              <Card.Title>Prettier</Card.Title>
              <Card.Text>Javascript formatter</Card.Text>
            </Card>
            <Card
              as="a"
              href="https://react-bootstrap.github.io/"
              target="_blank"
            >
              <Card.Title>React Bootstrap</Card.Title>
              <Card.Text>React UI Components</Card.Text>
            </Card>
            <Card as="a" href="https://aws.amazon.com/" target="_blank">
              <Card.Title>AWS</Card.Title>
              <Card.Text>Hosting, Amplify, Elastic Beanstalk, RDS</Card.Text>
            </Card>
            <Card
              as="a"
              href="https://cloud.google.com/appengine"
              target="_blank"
            >
              <Card.Title>GCP App Engine</Card.Title>
              <Card.Text>Staging our backend for acceptance tests</Card.Text>
            </Card>
            <Card as="a" href="https://www.postman.com/" target="_blank">
              <Card.Title>Postman</Card.Title>
              <Card.Text>API Design and Documentation</Card.Text>
            </Card>
            <Card as="a" href="https://www.docker.com/" target="_blank">
              <Card.Title>Docker</Card.Title>
              <Card.Text>Development Environment Containers</Card.Text>
            </Card>
            <Card as="a" href="https://www.namecheap.com/" target="_blank">
              <Card.Title>Namecheap</Card.Title>
              <Card.Text>Domain Registry</Card.Text>
            </Card>
            <Card
              as="a"
              href="https://flask.palletsprojects.com/en/2.0.x/"
              target="_blank"
            >
              <Card.Title>Flask</Card.Title>
              <Card.Text>Backend Server</Card.Text>
            </Card>
            <Card
              as="a"
              href="https://flask-restless-ng.readthedocs.io/en/latest/"
              target="_blank"
            >
              <Card.Title>Flask-Restless-NG</Card.Title>
              <Card.Text>API Endpoints, Filtering, Sorting</Card.Text>
            </Card>
            <Card as="a" href="https://gunicorn.org/" target="_blank">
              <Card.Title>Gunicorn</Card.Title>
              <Card.Text>Web Server Connected to Flask</Card.Text>
            </Card>
            <Card as="a" href="https://www.sqlalchemy.org/" target="_blank">
              <Card.Title>SQLAlchemy</Card.Title>
              <Card.Text>Database and Schema Integration</Card.Text>
            </Card>
            <Card
              as="a"
              href="https://flask-sqlalchemy.palletsprojects.com/en/2.x/"
              target="_blank"
            >
              <Card.Title>Flask-SQLAlchemy</Card.Title>
              <Card.Text>SQLAlchemy Simplifier</Card.Text>
            </Card>
            <Card as="a" href="https://www.postgresql.org/" target="_blank">
              <Card.Title>PostgreSQL</Card.Title>
              <Card.Text>Database</Card.Text>
            </Card>
            <Card as="a" href="https://www.psycopg.org/docs/" target="_blank">
              <Card.Title>Psycopg2</Card.Title>
              <Card.Text>Database Communication Driver</Card.Text>
            </Card>
            <Card
              as="a"
              href="https://black.readthedocs.io/en/stable/"
              target="_blank"
            >
              <Card.Title>Black</Card.Title>
              <Card.Text>Python Code Formatter</Card.Text>
            </Card>
            <Card as="a" href="http://mypy-lang.org/" target="_blank">
              <Card.Title>MyPy</Card.Title>
              <Card.Text>Static Type Checker</Card.Text>
            </Card>
            <Card as="a" href="https://pylint.org/" target="_blank">
              <Card.Title>PyLint</Card.Title>
              <Card.Text>Python Linter</Card.Text>
            </Card>
            <Card as="a" href="https://jestjs.io/" target="_blank">
              <Card.Title>Jest</Card.Title>
              <Card.Text>Test Javascript</Card.Text>
            </Card>
            <Card as="a" href="https://prettier.io/" target="_blank">
              <Card.Title>Prettier</Card.Title>
              <Card.Text>Javascript Formatter</Card.Text>
            </Card>
            <Card as="a" href="https://eslint.org/" target="_blank">
              <Card.Title>ESLint</Card.Title>
              <Card.Text>Javascript Linter</Card.Text>
            </Card>
            <Card as="a" href="https://www.selenium.dev/" target="_blank">
              <Card.Title>Selenium</Card.Title>
              <Card.Text>GUI Testing</Card.Text>
            </Card>
            <Card
              as="a"
              href="https://github.com/weknowinc/react-bubble-chart-d3"
              target="_blank"
            >
              <Card.Title>React-Bubble-Chart-D3</Card.Title>
              <Card.Text>Bubble Charts for Visualizations</Card.Text>
            </Card>
            <Card as="a" href="https://recharts.org/en-US/" target="_blank">
              <Card.Title>Recharts</Card.Title>
              <Card.Text>Graphs for Visualizations</Card.Text>
            </Card>
          </div>
          <div className={styles.documents}>
            <h1>GitLab and Postman</h1>
            <Card
              as="a"
              href="https://gitlab.com/cs373-11am-group-6/trailmix"
              target="_blank"
            >
              <Card.Title>TrailMix GitLab</Card.Title>
            </Card>
            <Card
              as="a"
              href="https://documenter.getpostman.com/view/17711210/UUy4cR54"
              target="_blank"
            >
              <Card.Title>TrailMix Postman</Card.Title>
            </Card>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default About;
