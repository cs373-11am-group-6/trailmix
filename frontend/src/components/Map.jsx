import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";
import React from "react";

const Map = withScriptjs(
  withGoogleMap((props) => {
    const { latitude, longitude } = props;

    return (
      <GoogleMap
        defaultZoom={8}
        defaultCenter={{ lat: latitude, lng: longitude }}
      >
        <Marker position={{ lat: latitude, lng: longitude }} />
      </GoogleMap>
    );
  })
);

export default Map;
