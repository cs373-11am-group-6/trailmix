import React, { useState, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  ListGroup,
  Form,
  Button,
  Table,
} from "react-bootstrap";
import getTrails from "../services/getTrails";
import getTrailsSearch from "../services/getTrailsSearch";

import getNationalParks from "../services/getNationalParks";
import getParksSearch from "../services/getParksSearch";

import getLodges from "../services/getLodges";
import getLodgesSearch from "../services/getLodgesSearch";

import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faSpinner } from "@fortawesome/free-solid-svg-icons";

const SearchPage = (props) => {
  const instancesPerPage = 5;

  const [trails, setTrails] = useState({});
  const [trailsLoaded, setTrailsLoaded] = useState(false);

  const [lodges, setLodges] = useState({});
  const [lodgesLoaded, setLodgesLoaded] = useState(false);

  const [nationalParks, setNationalParks] = useState({});
  const [nationalParksLoaded, setNationalParksLoaded] = useState(false);

  const [searchTerm, setSearchTerm] = useState("");

  useEffect(() => {
    getInitialInstances();
  }, []);

  const getInitialInstances = () => {
    getTrails(instancesPerPage, 1)
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setTrails(response);
        setTrailsLoaded(true);
      });

    getNationalParks(instancesPerPage, 1)
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setNationalParks(response);
        setNationalParksLoaded(true);
      });

    getLodges(instancesPerPage, 1)
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setLodges(response);
        setLodgesLoaded(true);
      });
  };

  const changeSearchTerm = (event) => {
    const search = event.target.value;
    setSearchTerm(search);
  };

  const search = () => {
    console.log(searchTerm);
    setTrailsLoaded(false);
    setLodgesLoaded(false);
    setNationalParksLoaded(false);

    getTrailsSearch(
      searchTerm,
      "",
      "",
      "",
      "",
      "",
      "",
      instancesPerPage,
      1,
      "name"
    )
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setTrails(response);
        setTrailsLoaded(true);
      });

    getParksSearch(searchTerm, "", instancesPerPage, 1, "name")
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setNationalParks(response);
        setNationalParksLoaded(true);
      });

    getLodgesSearch(searchTerm, "", "", "", "", instancesPerPage, 1, "name")
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setLodges(response);
        setLodgesLoaded(true);
      });
  };

  const loadTrailSplashPage = () => {
    if (searchTerm != "") {
      props.history.push({
        pathname: `/trails`,
        state: {
          sitewideSearchTerm: searchTerm,
        },
      });
    } else {
      props.history.push({
        pathname: `/trails`,
      });
    }
  };

  const loadParkSplashPage = () => {
    if (searchTerm != "") {
      props.history.push({
        pathname: `/parks`,
        state: {
          sitewideSearchTerm: searchTerm,
        },
      });
    } else {
      props.history.push({
        pathname: `/parks`,
      });
    }
  };

  const loadLodgesSplashPage = () => {
    if (searchTerm != "") {
      props.history.push({
        pathname: `/lodges`,
        state: {
          sitewideSearchTerm: searchTerm,
        },
      });
    } else {
      props.history.push({
        pathname: `/lodges`,
      });
    }
  };

  const loadTrailPage = (data, name) => {
    props.history.push({
      pathname: `/trails/view/${name}`,
      state: { data: data },
    });
  };

  const loadParkPage = (id) => {
    props.history.push({
      pathname: `/parks/view/${id}`,
    });
  };

  const loadLodgePage = (data, id) => {
    props.history.push({
      pathname: `/lodges/view/${id}`,
      state: { data: data },
    });
  };

  const searchHighlight = (attribute) => {
    if (
      searchTerm.length > 0 &&
      attribute.toUpperCase().includes(searchTerm.toUpperCase())
    ) {
      let searchTermLength = searchTerm.length;
      let attributeString = attribute;
      let attributeArr = [];

      while (attributeString.length > 0) {
        let index = attributeString
          .toUpperCase()
          .indexOf(searchTerm.toUpperCase());

        if (index != -1) {
          let nextString = attributeString.substring(0, index);
          attributeArr.push(nextString);
          let endOfSearchIndex = index + searchTermLength;
          attributeArr.push(attributeString.substring(index, endOfSearchIndex));
          attributeString = attributeString.substring(endOfSearchIndex);
        } else {
          // searchTerm not found in remaining string
          attributeArr.push(attributeString);
          attributeString = "";
        }
      }

      return (
        <div className="d-inline">
          {attributeArr.map((attr) => {
            if (attr.toUpperCase() == searchTerm.toUpperCase()) {
              return (
                <div className="d-inline" style={{ background: "#95e3d3" }}>
                  {attr}
                </div>
              );
            } else {
              return <div className="d-inline">{attr}</div>;
            }
          })}
        </div>
      );
    } else {
      return <div className="d-inline">{attribute}</div>;
    }
  };

  return (
    <div className="Container">
      <Container>
        <h1>Search</h1>
        <hr className="dark-line" />
        <Form>
          <Form.Group className="mb-3">
            <Row>
              <Col>
                <Form.Control
                  type="text"
                  placeholder="Enter query here..."
                  className="d-inline"
                  onChange={(e) => {
                    changeSearchTerm(e);
                  }}
                />
                <Button
                  variant="secondary"
                  className="d-inline"
                  onClick={search}
                  style={{ marginLeft: "10px" }}
                >
                  Search{" "}
                  <FontAwesomeIcon icon={faSearch} size="1x" color="white" />
                </Button>
              </Col>
            </Row>
          </Form.Group>
        </Form>

        <h2>Hike & Bike Trails</h2>
        <Container fluid={true}>
          <Row>
            {trailsLoaded && trails && trails["data"] ? (
              trails["data"].map((trail, index) => {
                return (
                  <Col key={index} className="mb-4" xs={11} md={4}>
                    <Card
                      text="dark"
                      border="dark"
                      className="hover-class"
                      onClick={() => {
                        loadTrailPage(trail, trail["attributes"]["name"]);
                      }}
                    >
                      <Card.Img
                        variant="top"
                        src={trail["attributes"]["image"]}
                      />
                      <Card.Body>
                        <Card.Title className="Title mt-4">
                          <strong>
                            {searchHighlight(trail["attributes"]["name"])}
                          </strong>
                        </Card.Title>
                        <Card.Subtitle className="text-muted Title mb-4">
                          {searchHighlight(trail["attributes"]["title"])}
                        </Card.Subtitle>
                        <ListGroup variant="list-group-flush">
                          {trail["attributes"]["states"] != "" && (
                            <ListGroup.Item>
                              <div className="d-inline">
                                <strong>States:</strong>{" "}
                              </div>
                              {searchHighlight(trail["attributes"]["states"])}
                            </ListGroup.Item>
                          )}
                          {trail["attributes"]["difficulty"] != "Unknown" &&
                            trail["attributes"]["difficulty"] != "" && (
                              <ListGroup.Item>
                                <div className="d-inline">
                                  <strong>Difficulty:</strong>{" "}
                                </div>
                                {searchHighlight(
                                  trail["attributes"]["difficulty"]
                                )}
                              </ListGroup.Item>
                            )}
                          {trail["attributes"]["distance"] != 0 && (
                            <ListGroup.Item>
                              <strong>Distance:</strong>{" "}
                              {trail["attributes"]["distance"]} miles
                            </ListGroup.Item>
                          )}
                          <ListGroup.Item>
                            <strong>Reservation Required?</strong>{" "}
                            {trail["attributes"]["reservation"] ? "Yes" : "No"}
                          </ListGroup.Item>
                          <ListGroup.Item>
                            <strong>Fees?</strong>{" "}
                            {trail["attributes"]["fees"] ? "Yes" : "No"}
                          </ListGroup.Item>
                          <ListGroup.Item>
                            <strong>Pets Allowed?</strong>{" "}
                            {trail["attributes"]["pets"] ? "Yes" : "No"}
                          </ListGroup.Item>
                        </ListGroup>
                      </Card.Body>
                    </Card>
                  </Col>
                );
              })
            ) : (
              <tr>
                <td>
                  <h2>
                    <FontAwesomeIcon icon={faSpinner} spin />
                  </h2>
                </td>
              </tr>
            )}
            {trailsLoaded && trails && trails["data"] ? (
              <Col className="mb-4" xs={11} md={4}>
                <Card
                  text="dark"
                  border="dark"
                  className="hover-class"
                  onClick={() => {
                    loadTrailSplashPage();
                  }}
                >
                  <Card.Body>
                    <Card.Title className="Title mt-4">
                      <strong>
                        Click to find all search results for this model
                      </strong>
                    </Card.Title>
                  </Card.Body>
                </Card>
              </Col>
            ) : (
              ""
            )}
          </Row>
        </Container>

        <h2>National Parks</h2>
        <Container fluid={true}>
          <Row>
            {nationalParksLoaded && nationalParks && nationalParks["data"] ? (
              nationalParks["data"].map((park, index) => {
                return (
                  <Col key={index} className="mb-4" xs={11} md={4}>
                    <Card
                      text="dark"
                      border="dark"
                      className="hover-class"
                      onClick={() => {
                        loadParkPage(park["id"]);
                      }}
                    >
                      <Card.Img
                        variant="top"
                        src={park["attributes"]["image"]}
                      />
                      <Card.Body>
                        <Card.Title className="Title mt-4">
                          <strong>
                            {searchHighlight(park["attributes"]["name"])}
                          </strong>
                        </Card.Title>
                        <Card.Subtitle className="text-muted Title mb-4">
                          {park["attributes"]["states"]}
                        </Card.Subtitle>
                        <ListGroup variant="list-group-flush">
                          <ListGroup.Item>
                            <strong>Park Code:</strong>{" "}
                            {searchHighlight(park["attributes"]["parkCode"])}
                          </ListGroup.Item>
                          <ListGroup.Item>
                            <strong>Designation:</strong>{" "}
                            {searchHighlight(park["attributes"]["designation"])}
                          </ListGroup.Item>
                          <ListGroup.Item>
                            <strong>Max Entry Fee:</strong>{" "}
                            {"$" +
                              String(
                                Number(park["attributes"]["max_fee"]).toFixed(2)
                              )}
                          </ListGroup.Item>
                          <ListGroup.Item>
                            <strong>Number of Activities:</strong>{" "}
                            {park["attributes"]["activities_cnt"]}
                          </ListGroup.Item>
                        </ListGroup>
                      </Card.Body>
                    </Card>
                  </Col>
                );
              })
            ) : (
              <tr>
                <td>
                  <h2>
                    <FontAwesomeIcon icon={faSpinner} spin />
                  </h2>
                </td>
              </tr>
            )}
            {nationalParksLoaded && nationalParks && nationalParks["data"] ? (
              <Col className="mb-4" xs={11} md={4}>
                <Card
                  text="dark"
                  border="dark"
                  className="hover-class"
                  onClick={() => {
                    loadParkSplashPage();
                  }}
                >
                  <Card.Body>
                    <Card.Title className="Title mt-4">
                      <strong>
                        Click to find all search results for this model
                      </strong>
                    </Card.Title>
                  </Card.Body>
                </Card>
              </Col>
            ) : (
              ""
            )}
          </Row>
        </Container>

        <h2>Lodges</h2>
        <Container fluid={true}>
          {lodgesLoaded && lodges && lodges["data"] ? (
            <Table
              variant="light"
              responsive
              striped
              bordered
              hover
              className="lodges-table"
            >
              <thead>
                <tr>
                  <th>Name</th>
                  <th>City</th>
                  <th>State</th>
                  <th>Fees</th>
                  <th>Lodging Type</th>
                  <th>Reservable</th>
                </tr>
              </thead>
              <tbody>
                {lodges["data"].map((lodge, index) => {
                  return (
                    <tr
                      key={index}
                      onClick={() => {
                        loadLodgePage(lodge, lodge["id"]);
                      }}
                    >
                      <td>
                        {
                          <strong>
                            {searchHighlight(lodge["attributes"]["name"])}
                          </strong>
                        }
                      </td>
                      <td>
                        {searchHighlight(lodge["attributes"]["location"])}
                      </td>

                      <td>{searchHighlight(lodge["attributes"]["state"])}</td>
                      <td>
                        {searchHighlight(
                          lodge["attributes"]["fee_description"] !=
                            "No fees specified"
                            ? "Yes"
                            : "No"
                        )}
                      </td>
                      <td>
                        {searchHighlight(lodge["attributes"]["lodging_type"])}
                      </td>
                      <td>{String(lodge["attributes"]["reservable"])}</td>
                    </tr>
                  );
                })}{" "}
              </tbody>
            </Table>
          ) : (
            <h2>
              <FontAwesomeIcon icon={faSpinner} spin />
            </h2>
          )}

          {lodgesLoaded && lodges && lodges["data"] ? (
            <Col className="mb-4" xs={11} md={4}>
              <Card
                text="dark"
                border="dark"
                className="hover-class"
                onClick={() => {
                  loadLodgesSplashPage();
                }}
              >
                <Card.Body>
                  <Card.Title className="Title mt-4">
                    <strong>
                      Click to find all search results for this model
                    </strong>
                  </Card.Title>
                </Card.Body>
              </Card>
            </Col>
          ) : (
            ""
          )}
        </Container>
      </Container>
    </div>
  );
};

SearchPage.propTypes = {
  history: PropTypes.object,
};

export default SearchPage;
