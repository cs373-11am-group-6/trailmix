import React, { useState, useEffect } from "react";
import {
  Pagination,
  Container,
  Row,
  Col,
  Card,
  ListGroup,
  Form,
  Button,
  FloatingLabel,
} from "react-bootstrap";
import getTrails from "../services/getTrails";
import getTrailsSearch from "../services/getTrailsSearch";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faSpinner } from "@fortawesome/free-solid-svg-icons";

const TrailsPage = (props) => {
  const trailsPerPage = 30;
  const states = [
    "Alabama - AL",
    "Alaska - AK",
    "Arizona - AZ",
    "Arkansas - AR",
    "California - CA",
    "Colorado - CO",
    "Connecticut - CT",
    "Delaware - DE",
    "Florida - FL",
    "Georgia - GA",
    "Hawaii - HI",
    "Idaho - ID",
    "Illinois - IL",
    "Indiana - IN",
    "Iowa - IA",
    "Kansas - KS",
    "Kentucky - KY",
    "Louisiana - LA",
    "Maine - ME",
    "Maryland - MD",
    "Massachusetts - MA",
    "Michigan - MI",
    "Minnesota - MN",
    "Mississippi - MS",
    "Missouri - MO",
    "Montana - MT",
    "Nebraska - NE",
    "Nevada - NV",
    "New Hampshire - NH",
    "New Jersey - NJ",
    "New Mexico - NM",
    "New York - NY",
    "North Carolina - NC",
    "North Dakota - ND",
    "Ohio - OH",
    "Oklahoma - OK",
    "Oregon - OR",
    "Pennsylvania - PA",
    "Rhode Island - RI",
    "South Carolina - SC",
    "South Dakota - SD",
    "Tennessee - TN",
    "Texas - TX",
    "Utah - UT",
    "Vermont - VT",
    "Virginia - VA",
    "Washington - WA",
    "West Virginia - WV",
    "Wisconsin - WI",
    "Wyoming - WY",
  ];

  const [trails, setTrails] = useState({});
  const [trailsLoaded, setTrailsLoaded] = useState(false);
  const [totalTrails, setTotalTrails] = useState(1);
  const [totalPageCount, setTotalPageCount] = useState(1);
  let [currentPage, setCurrentPage] = useState(1);

  const [searchTerm, setSearchTerm] = useState("");
  const [stateFilter, setStateFilter] = useState("");
  const [reservationFilter, setReservationFilter] = useState("");
  const [feesFilter, setFeesFilter] = useState("");
  const [petsFilter, setPetsFilter] = useState("");
  const [difficultyFilter, setDifficultyFilter] = useState("");
  const [distanceFilter, setDistanceFilter] = useState("");
  const [sortKey, setSortKey] = useState("name");

  const [sitewideSearchTermUsed, setSitewideSearchTermUsed] = useState(false);

  useEffect(() => {
    if (props.location.state == null) {
      getTrailsAtPage(1);
    } else {
      setSearchTerm(props.location.state.sitewideSearchTerm);
      setSitewideSearchTermUsed(true);
    }
  }, []);

  // to make sitewide searching work
  useEffect(() => {
    if (sitewideSearchTermUsed) {
      search(1);
    }
  }, [sitewideSearchTermUsed]);

  const loadTrailPage = (data, name) => {
    props.history.push({
      pathname: `/trails/view/${name}`,
      state: { data: data },
    });
  };

  const getTrailsAtPage = (currentPage) => {
    console.log(sortKey);
    getTrails(trailsPerPage, currentPage, sortKey)
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setTrails(response);
        setTrailsLoaded(true);

        if (currentPage == 1) {
          updatePagination(response);
        }
      });
  };

  const loadNewTrailsPage = (currentPage) => {
    getFilteredTrails(currentPage);
  };

  const reset = () => {
    setSortKey("name");
    setSearchTerm("");
    setStateFilter("");
    setReservationFilter("");
    setFeesFilter("");
    setPetsFilter("");
    setDifficultyFilter("");
    setDistanceFilter("");
    setTrailsLoaded(false);
  };

  const changeSearchTerm = (event) => {
    const search = event.target.value;
    setSearchTerm(search);
  };

  const search = (currentPage) => {
    if (isNaN(currentPage)) {
      currentPage = 1;
    }

    if (searchTerm.length > 0) {
      // getSearchedTrails(currentPage);
      getFilteredTrails(currentPage);
    }
  };

  const getFilteredTrails = (currentPage) => {
    setTrailsLoaded(false);

    const state = stateFilter.length > 0 ? stateFilter.split(" - ")[1] : "";

    getTrailsSearch(
      searchTerm,
      state,
      reservationFilter,
      feesFilter,
      petsFilter,
      difficultyFilter,
      distanceFilter,
      trailsPerPage,
      currentPage,
      sortKey
    )
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setTrails(response);
        setTrailsLoaded(true);

        if (currentPage == 1) {
          updatePagination(response);
        }
      });
  };

  const filter = () => {
    getFilteredTrails(1);
  };

  const updateStateFilter = (event) => {
    const stateFilter = event.target.value;
    setStateFilter(stateFilter);
  };

  const updateReservationFilter = (event) => {
    const reservationFilter = event.target.value;
    setReservationFilter(reservationFilter);
  };

  const updateFeesFilter = (event) => {
    const feesFilter = event.target.value;
    setFeesFilter(feesFilter);
  };

  const updatePetsFilter = (event) => {
    const petsFilter = event.target.value;
    setPetsFilter(petsFilter);
  };

  const updateDifficultyFilter = (event) => {
    const difficultyFilter = event.target.value;
    setDifficultyFilter(difficultyFilter);
  };

  const updateDistanceFilter = (event) => {
    const distanceFilter = event.target.value;
    setDistanceFilter(distanceFilter);
  };

  const updateSortKey = (event) => {
    const sortKey = event.target.value;
    setSortKey(sortKey);
    console.log(sortKey);
  };

  const updatePagination = (response) => {
    if (response && response["meta"]) {
      // error checking data was returned
      setTotalTrails(response["meta"]["total"]);
      setTotalPageCount(
        response["meta"]["total"] % trailsPerPage == 0
          ? Math.floor(response["meta"]["total"] / trailsPerPage)
          : Math.floor(response["meta"]["total"] / trailsPerPage) + 1
      );
      setCurrentPage(1);
    }
  };

  const goToPage = (event) => {
    currentPage = event.target.text;
    setCurrentPage(currentPage);
    setTrailsLoaded(false);

    loadNewTrailsPage(currentPage);
  };

  const nextPage = () => {
    currentPage = parseInt(currentPage) + 1;
    setCurrentPage(currentPage);
    setTrailsLoaded(false);

    loadNewTrailsPage(currentPage);
  };

  const lastPage = () => {
    currentPage = totalPageCount;
    setCurrentPage(currentPage);
    setTrailsLoaded(false);

    loadNewTrailsPage(currentPage);
  };

  const backPage = () => {
    currentPage = parseInt(currentPage) - 1;
    setCurrentPage(currentPage);
    setTrailsLoaded(false);

    loadNewTrailsPage(currentPage);
  };

  const firstPage = () => {
    currentPage = 1;
    setCurrentPage(currentPage);
    setTrailsLoaded(false);

    loadNewTrailsPage(currentPage);
  };

  const pagination = () => {
    if (trailsLoaded) {
      return (
        <div className="grid-footer">
          <Pagination className="float-right">
            {currentPage != 1 && (
              <Pagination.First onClick={() => firstPage()} />
            )}
            {currentPage != 1 && <Pagination.Prev onClick={() => backPage()} />}

            {currentPage == 1 && <Pagination.First disabled />}
            {currentPage == 1 && <Pagination.Prev disabled />}

            {currentPage != 1 && (
              <Pagination.Item onClick={(event) => goToPage(event)}>
                {parseInt(currentPage) - 1}
              </Pagination.Item>
            )}
            <Pagination.Item className="pagination activeCell">
              {currentPage}
            </Pagination.Item>

            {currentPage != totalPageCount && (
              <Pagination.Item onClick={(event) => goToPage(event)}>
                {parseInt(currentPage) + 1}
              </Pagination.Item>
            )}

            {currentPage == totalPageCount && <Pagination.Next disabled />}
            {currentPage == totalPageCount && <Pagination.Last disabled />}

            {currentPage != totalPageCount &&
              parseInt(currentPage) + 1 != totalPageCount && (
                <Pagination.Ellipsis disabled />
              )}
            {currentPage != totalPageCount &&
              parseInt(currentPage) + 1 != totalPageCount && (
                <Pagination.Item onClick={(event) => goToPage(event)}>
                  {totalPageCount}
                </Pagination.Item>
              )}

            {currentPage != totalPageCount && (
              <Pagination.Next onClick={() => nextPage()} />
            )}
            {currentPage != totalPageCount && (
              <Pagination.Last onClick={() => lastPage()} />
            )}
          </Pagination>
          <h5>
            <strong>{totalTrails} RESULTS</strong>
          </h5>
        </div>
      );
    }
  };

  const searchHighlight = (attribute) => {
    if (
      searchTerm.length > 0 &&
      attribute.toUpperCase().includes(searchTerm.toUpperCase())
    ) {
      let searchTermLength = searchTerm.length;
      let attributeString = attribute;
      let attributeArr = [];

      while (attributeString.length > 0) {
        let index = attributeString
          .toUpperCase()
          .indexOf(searchTerm.toUpperCase());

        if (index != -1) {
          let nextString = attributeString.substring(0, index);
          attributeArr.push(nextString);
          let endOfSearchIndex = index + searchTermLength;
          attributeArr.push(attributeString.substring(index, endOfSearchIndex));
          attributeString = attributeString.substring(endOfSearchIndex);
        } else {
          // searchTerm not found in remaining string
          attributeArr.push(attributeString);
          attributeString = "";
        }
      }

      return (
        <div className="d-inline">
          {attributeArr.map((attr) => {
            if (attr.toUpperCase() == searchTerm.toUpperCase()) {
              return (
                <div className="d-inline" style={{ background: "#95e3d3" }}>
                  {attr}
                </div>
              );
            } else {
              return <div className="d-inline">{attr}</div>;
            }
          })}
        </div>
      );
    } else {
      return <div className="d-inline">{attribute}</div>;
    }
  };

  return (
    <div className="Container">
      <Container>
        <h1>Hike & Bike Trails</h1>
        <hr className="dark-line" />
        <Form>
          <Form.Group className="mb-3">
            <Row>
              <Col>
                <Form.Control
                  type="text"
                  placeholder="Enter query here..."
                  className="d-inline"
                  value={searchTerm}
                  onChange={(e) => {
                    changeSearchTerm(e);
                  }}
                />
                <Button
                  variant="secondary"
                  className="d-inline"
                  onClick={search}
                  style={{ marginLeft: "10px" }}
                >
                  Search{" "}
                  <FontAwesomeIcon icon={faSearch} size="1x" color="white" />
                </Button>
              </Col>
            </Row>
          </Form.Group>

          <Form.Group className="mb-3">
            <Row>
              <Col md={3}>
                <FloatingLabel
                  label="State"
                  style={{ color: "black", marginRight: "10px" }}
                >
                  <Form.Select
                    value={stateFilter}
                    onChange={(e) => {
                      updateStateFilter(e);
                    }}
                  >
                    <option value="">Choose...</option>
                    {states.map((state, index) => {
                      return (
                        <option key={index} value={state}>
                          {state}
                        </option>
                      );
                    })}
                  </Form.Select>
                </FloatingLabel>
              </Col>
              <Col md={6} style={{ marginLeft: "10px", marginBottom: "10px" }}>
                <Form.Group>
                  <Form.Label style={{ width: "80px" }}>Reserve?</Form.Label>
                  <Form.Check
                    inline
                    type="radio"
                    label="Yes"
                    checked={reservationFilter == "true" ? true : false}
                    onChange={updateReservationFilter}
                    style={{ minWidth: "0px" }}
                    name="reserveGroup"
                    value="true"
                  />
                  <Form.Check
                    inline
                    type="radio"
                    label="No"
                    checked={reservationFilter == "false" ? true : false}
                    onChange={updateReservationFilter}
                    style={{ minWidth: "0px" }}
                    name="reserveGroup"
                    value="false"
                  />
                  <Form.Check
                    inline
                    type="radio"
                    label="N/A"
                    checked={reservationFilter == "" ? true : false}
                    onChange={updateReservationFilter}
                    style={{ minWidth: "0px" }}
                    name="reserveGroup"
                    value=""
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label style={{ width: "80px" }}>Fees?</Form.Label>
                  <Form.Check
                    inline
                    type="radio"
                    label="Yes"
                    checked={feesFilter == "true" ? true : false}
                    onChange={updateFeesFilter}
                    style={{ minWidth: "0px" }}
                    value="true"
                    name="feesGroup"
                  />
                  <Form.Check
                    inline
                    type="radio"
                    label="No"
                    checked={feesFilter == "false" ? true : false}
                    onChange={updateFeesFilter}
                    style={{ minWidth: "0px" }}
                    value="false"
                    name="feesGroup"
                  />
                  <Form.Check
                    inline
                    type="radio"
                    label="N/A"
                    checked={feesFilter == "" ? true : false}
                    onChange={updateFeesFilter}
                    style={{ minWidth: "0px" }}
                    value=""
                    name="feesGroup"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label style={{ width: "80px" }}>Pets?</Form.Label>
                  <Form.Check
                    inline
                    type="radio"
                    label="Yes"
                    checked={petsFilter == "true" ? true : false}
                    onChange={updatePetsFilter}
                    style={{ minWidth: "0px" }}
                    value="true"
                    name="petsGroup"
                  />
                  <Form.Check
                    inline
                    type="radio"
                    label="No"
                    checked={petsFilter == "false" ? true : false}
                    onChange={updatePetsFilter}
                    style={{ minWidth: "0px" }}
                    value="false"
                    name="petsGroup"
                  />
                  <Form.Check
                    inline
                    type="radio"
                    label="N/A"
                    checked={petsFilter == "" ? true : false}
                    onChange={updatePetsFilter}
                    style={{ minWidth: "0px" }}
                    value=""
                    name="petsGroup"
                  />
                </Form.Group>
              </Col>
              <Col md={3}>
                <FloatingLabel
                  label="Difficulty"
                  style={{
                    color: "black",
                    marginRight: "10px",
                    width: "150px",
                  }}
                >
                  <Form.Select
                    value={difficultyFilter}
                    onChange={(e) => {
                      updateDifficultyFilter(e);
                    }}
                  >
                    <option value="">Choose...</option>
                    <option value="Easy">Easy</option>
                    <option value="Moderate">Moderate</option>
                    <option value="Strenuous">Strenuous</option>
                  </Form.Select>
                </FloatingLabel>
              </Col>
              <Col md={3}>
                <FloatingLabel
                  label="Sort by"
                  style={{
                    color: "black",
                    marginRight: "10px",
                    width: "150px",
                  }}
                >
                  <Form.Select
                    value={sortKey}
                    onChange={(e) => {
                      updateSortKey(e);
                    }}
                  >
                    <option value="name">Name (A-Z)</option>
                    <option value="-name">Name (Z-A)</option>
                    <option value="distance,name">Dist (Asc.)</option>
                    <option value="-distance,name">Dist (Desc.)</option>
                  </Form.Select>
                </FloatingLabel>
                <Form.Control
                  placeholder="Distance (mi)"
                  style={{ width: "150px" }}
                  className="d-inline"
                  value={distanceFilter}
                  onChange={(e) => {
                    updateDistanceFilter(e);
                  }}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Button
                  variant="secondary"
                  style={{
                    width: "170px",
                    marginLeft: "10px",
                    marginBottom: "10px",
                  }}
                  onClick={filter}
                >
                  Filter & Sort
                </Button>
                <Button
                  variant="secondary"
                  style={{ marginLeft: "10px", marginBottom: "10px" }}
                  onClick={() => {
                    reset();
                    getTrailsAtPage(1);
                  }}
                >
                  Reset
                </Button>
              </Col>
            </Row>
          </Form.Group>
        </Form>

        <Container fluid={true}>
          <Row>
            {trailsLoaded && trails && trails["data"] ? (
              trails["data"].map((trail, index) => {
                return (
                  <Col key={index} className="mb-4" xs={11} md={4}>
                    <Card
                      text="dark"
                      border="dark"
                      className="hover-class"
                      onClick={() => {
                        loadTrailPage(trail, trail["attributes"]["name"]);
                      }}
                    >
                      <Card.Img
                        variant="top"
                        src={trail["attributes"]["image"]}
                      />
                      <Card.Body>
                        <Card.Title className="Title mt-4">
                          <strong>
                            {searchHighlight(trail["attributes"]["name"])}
                          </strong>
                        </Card.Title>
                        <Card.Subtitle className="text-muted Title mb-4">
                          {searchHighlight(trail["attributes"]["title"])}
                        </Card.Subtitle>
                        <ListGroup variant="list-group-flush">
                          {trail["attributes"]["states"] != "" && (
                            <ListGroup.Item>
                              <div className="d-inline">
                                <strong>States:</strong>{" "}
                              </div>
                              {searchHighlight(trail["attributes"]["states"])}
                            </ListGroup.Item>
                          )}
                          {trail["attributes"]["difficulty"] != "Unknown" &&
                            trail["attributes"]["difficulty"] != "" && (
                              <ListGroup.Item>
                                <div className="d-inline">
                                  <strong>Difficulty:</strong>{" "}
                                </div>
                                {searchHighlight(
                                  trail["attributes"]["difficulty"]
                                )}
                              </ListGroup.Item>
                            )}
                          {trail["attributes"]["distance"] != 0 && (
                            <ListGroup.Item>
                              <strong>Distance:</strong>{" "}
                              {trail["attributes"]["distance"]} miles
                            </ListGroup.Item>
                          )}
                          <ListGroup.Item>
                            <strong>Reservation Required?</strong>{" "}
                            {trail["attributes"]["reservation"] ? "Yes" : "No"}
                          </ListGroup.Item>
                          <ListGroup.Item>
                            <strong>Fees?</strong>{" "}
                            {trail["attributes"]["fees"] ? "Yes" : "No"}
                          </ListGroup.Item>
                          <ListGroup.Item>
                            <strong>Pets Allowed?</strong>{" "}
                            {trail["attributes"]["pets"] ? "Yes" : "No"}
                          </ListGroup.Item>
                        </ListGroup>
                      </Card.Body>
                    </Card>
                  </Col>
                );
              })
            ) : (
              <tr>
                <td>
                  <h2>
                    <FontAwesomeIcon icon={faSpinner} spin />
                  </h2>
                </td>
              </tr>
            )}
          </Row>
        </Container>
        {pagination()}
      </Container>
    </div>
  );
};

TrailsPage.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object,
};

export default TrailsPage;
