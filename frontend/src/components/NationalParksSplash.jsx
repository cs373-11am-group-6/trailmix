import React, { useState, useEffect } from "react";
import {
  Pagination,
  Container,
  Row,
  Col,
  Card,
  ListGroup,
  Form,
  Button,
  FloatingLabel,
} from "react-bootstrap";
import getNationalParks from "../services/getNationalParks";
import getParksSearch from "../services/getParksSearch";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faSpinner } from "@fortawesome/free-solid-svg-icons";

const NationalParksHome = (props) => {
  const parksPerPage = 30;
  const states = [
    "Alabama - AL",
    "Alaska - AK",
    "Arizona - AZ",
    "Arkansas - AR",
    "California - CA",
    "Colorado - CO",
    "Connecticut - CT",
    "Delaware - DE",
    "Florida - FL",
    "Georgia - GA",
    "Hawaii - HI",
    "Idaho - ID",
    "Illinois - IL",
    "Indiana - IN",
    "Iowa - IA",
    "Kansas - KS",
    "Kentucky - KY",
    "Louisiana - LA",
    "Maine - ME",
    "Maryland - MD",
    "Massachusetts - MA",
    "Michigan - MI",
    "Minnesota - MN",
    "Mississippi - MS",
    "Missouri - MO",
    "Montana - MT",
    "Nebraska - NE",
    "Nevada - NV",
    "New Hampshire - NH",
    "New Jersey - NJ",
    "New Mexico - NM",
    "New York - NY",
    "North Carolina - NC",
    "North Dakota - ND",
    "Ohio - OH",
    "Oklahoma - OK",
    "Oregon - OR",
    "Pennsylvania - PA",
    "Rhode Island - RI",
    "South Carolina - SC",
    "South Dakota - SD",
    "Tennessee - TN",
    "Texas - TX",
    "Utah - UT",
    "Vermont - VT",
    "Virginia - VA",
    "Washington - WA",
    "West Virginia - WV",
    "Wisconsin - WI",
    "Wyoming - WY",
  ];

  const [nationalParks, setNationalParks] = useState({});
  const [nationalParksLoaded, setNationalParksLoaded] = useState(false);
  let [currentPage, setCurrentPage] = useState(1);
  const [totalParks, setTotalParks] = useState(1);
  const [totalPageCount, setTotalPageCount] = useState(1);

  const [searchTerm, setSearchTerm] = useState("");
  const [stateFilter, setStateFilter] = useState("");
  const [sortKey, setSortKey] = useState("name");
  const [sitewideSearchTermUsed, setSitewideSearchTermUsed] = useState(false);

  useEffect(() => {
    if (props.location.state == null) {
      getParksAtPage(1);
    } else {
      setSearchTerm(props.location.state.sitewideSearchTerm);
      setSitewideSearchTermUsed(true);
    }
  }, []);

  // to make sitewide searching work
  useEffect(() => {
    if (sitewideSearchTermUsed) {
      search(1);
    }
  }, [sitewideSearchTermUsed]);

  const loadParkPage = (id) => {
    props.history.push({
      pathname: `/parks/view/${id}`,
    });
  };

  const getParksAtPage = (currentPage) => {
    getNationalParks(parksPerPage, currentPage)
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setNationalParks(response);
        setNationalParksLoaded(true);

        if (currentPage == 1) {
          updatePagination(response);
        }
      });
  };

  const loadNewParksPage = (currentPage) => {
    if (searchTerm != "") {
      search(currentPage);
    } else if (stateFilter != "" || sortKey != "name") {
      getFilteredParks(currentPage);
    } else {
      getParksAtPage(currentPage);
    }
  };

  const reset = () => {
    setSearchTerm("");
    setStateFilter("");
    setNationalParksLoaded(false);
    setSortKey("name");
    getParksAtPage(1);
  };

  const changeSearchTerm = (event) => {
    const search = event.target.value;
    setSearchTerm(search);
  };

  const search = (currentPage) => {
    if (isNaN(currentPage)) {
      currentPage = 1;
    }

    if (searchTerm.length > 0) {
      getSearchedParks(currentPage);
    }
  };

  const getSearchedParks = (currentPage) => {
    setNationalParksLoaded(false);

    getParksSearch(searchTerm, "", parksPerPage, currentPage, sortKey)
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setNationalParks(response);
        setNationalParksLoaded(true);

        if (currentPage == 1) {
          updatePagination(response);
        }
      });
  };

  const getFilteredParks = (currentPage) => {
    setNationalParksLoaded(false);

    const state = stateFilter.length > 0 ? stateFilter.split(" - ")[1] : "";

    getParksSearch(searchTerm, state, parksPerPage, currentPage, sortKey)
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setNationalParks(response);
        setNationalParksLoaded(true);

        if (currentPage == 1) {
          updatePagination(response);
        }
      });
  };

  const filter = () => {
    getFilteredParks(1);
  };

  const updateStateFilter = (event) => {
    const stateFilter = event.target.value;
    console.log(stateFilter);
    setStateFilter(stateFilter);
  };

  const updateSortKey = (event) => {
    const sortKey = event.target.value;
    setSortKey(sortKey);
  };

  const updatePagination = (response) => {
    if (response && response["meta"]) {
      // error checking data was returned
      setTotalParks(response["meta"]["total"]);
      setTotalPageCount(
        response["meta"]["total"] % parksPerPage == 0
          ? Math.floor(response["meta"]["total"] / parksPerPage)
          : Math.floor(response["meta"]["total"] / parksPerPage) + 1
      );
      setCurrentPage(1);
    }
  };

  const goToPage = (event) => {
    currentPage = event.target.text;
    setCurrentPage(currentPage);
    setNationalParksLoaded(false);

    loadNewParksPage(currentPage);
  };

  const nextPage = () => {
    currentPage = parseInt(currentPage) + 1;
    setCurrentPage(currentPage);
    setNationalParksLoaded(false);

    loadNewParksPage(currentPage);
  };

  const lastPage = () => {
    currentPage = totalPageCount;
    setCurrentPage(currentPage);
    setNationalParksLoaded(false);

    loadNewParksPage(currentPage);
  };

  const backPage = () => {
    currentPage = parseInt(currentPage) - 1;
    setCurrentPage(currentPage);
    setNationalParksLoaded(false);

    loadNewParksPage(currentPage);
  };

  const firstPage = () => {
    currentPage = 1;
    setCurrentPage(currentPage);
    setNationalParksLoaded(false);

    loadNewParksPage(currentPage);
  };

  const pagination = () => {
    if (nationalParksLoaded) {
      return (
        <div className="grid-footer">
          <Pagination className="float-right">
            {currentPage != 1 && (
              <Pagination.First onClick={() => firstPage()} />
            )}
            {currentPage != 1 && <Pagination.Prev onClick={() => backPage()} />}

            {currentPage == 1 && <Pagination.First disabled />}
            {currentPage == 1 && <Pagination.Prev disabled />}

            {currentPage != 1 && (
              <Pagination.Item onClick={(event) => goToPage(event)}>
                {parseInt(currentPage) - 1}
              </Pagination.Item>
            )}
            <Pagination.Item className="pagination activeCell">
              {currentPage}
            </Pagination.Item>

            {currentPage != totalPageCount && (
              <Pagination.Item onClick={(event) => goToPage(event)}>
                {parseInt(currentPage) + 1}
              </Pagination.Item>
            )}

            {currentPage == totalPageCount && <Pagination.Next disabled />}
            {currentPage == totalPageCount && <Pagination.Last disabled />}

            {currentPage != totalPageCount &&
              parseInt(currentPage) + 1 != totalPageCount && (
                <Pagination.Ellipsis disabled />
              )}
            {currentPage != totalPageCount &&
              parseInt(currentPage) + 1 != totalPageCount && (
                <Pagination.Item onClick={(event) => goToPage(event)}>
                  {totalPageCount}
                </Pagination.Item>
              )}

            {currentPage != totalPageCount && (
              <Pagination.Next onClick={() => nextPage()} />
            )}
            {currentPage != totalPageCount && (
              <Pagination.Last onClick={() => lastPage()} />
            )}
          </Pagination>
          <h5>
            <strong>{totalParks} RESULTS</strong>
          </h5>
        </div>
      );
    }
  };

  const searchHighlight = (attribute) => {
    if (
      searchTerm.length > 0 &&
      attribute.toUpperCase().includes(searchTerm.toUpperCase())
    ) {
      let searchTermLength = searchTerm.length;
      let attributeString = attribute;
      let attributeArr = [];

      while (attributeString.length > 0) {
        let index = attributeString
          .toUpperCase()
          .indexOf(searchTerm.toUpperCase());

        if (index != -1) {
          let nextString = attributeString.substring(0, index);
          attributeArr.push(nextString);
          let endOfSearchIndex = index + searchTermLength;
          attributeArr.push(attributeString.substring(index, endOfSearchIndex));
          attributeString = attributeString.substring(endOfSearchIndex);
        } else {
          // searchTerm not found in remaining string
          attributeArr.push(attributeString);
          attributeString = "";
        }
      }

      return (
        <div className="d-inline">
          {attributeArr.map((attr) => {
            if (attr.toUpperCase() == searchTerm.toUpperCase()) {
              return (
                <div className="d-inline" style={{ background: "#95e3d3" }}>
                  {attr}
                </div>
              );
            } else {
              return <div className="d-inline">{attr}</div>;
            }
          })}
        </div>
      );
    } else {
      return <div className="d-inline">{attribute}</div>;
    }
  };

  return (
    <div className="Container">
      <Container>
        <h1>National Parks</h1>
        <hr className="dark-line" />
        <Form>
          <Form.Group className="mb-3">
            <Row>
              <Col>
                <Form.Control
                  type="text"
                  placeholder="Enter query here..."
                  className="d-inline"
                  value={searchTerm}
                  onChange={(e) => {
                    changeSearchTerm(e);
                  }}
                />
                <Button
                  variant="secondary"
                  className="d-inline"
                  onClick={search}
                  style={{ marginLeft: "10px" }}
                >
                  Search{" "}
                  <FontAwesomeIcon icon={faSearch} size="1x" color="white" />
                </Button>
              </Col>
            </Row>
          </Form.Group>

          <Form.Group className="mb-3">
            <Row>
              <Col md={3}>
                <FloatingLabel
                  label="State"
                  style={{ color: "black", marginRight: "10px" }}
                >
                  <Form.Select
                    value={stateFilter}
                    onChange={(e) => {
                      updateStateFilter(e);
                    }}
                  >
                    <option>Choose...</option>
                    {states.map((state, index) => {
                      return (
                        <option key={index} value={state}>
                          {state}
                        </option>
                      );
                    })}
                  </Form.Select>
                </FloatingLabel>
              </Col>
              <Col>
                <FloatingLabel
                  label="Sort"
                  style={{
                    color: "black",
                    marginRight: "10px",
                    width: "150px",
                  }}
                >
                  <Form.Select
                    value={sortKey}
                    onChange={(e) => {
                      updateSortKey(e);
                    }}
                  >
                    <option value="name">Name: A-Z</option>
                    <option value="-name">Name: Z-A</option>
                    <option value="parkCode">Park Code: A-Z</option>
                    <option value="-parkCode">Park Code: Z-A</option>
                    <option value="max_fee">Max Entry Fee (Asc.)</option>
                    <option value="-max_fee">Max Entry Fee (Desc.)</option>
                    <option value="activities_cnt">
                      Number of Activities (Asc.)
                    </option>
                    <option value="-activities_cnt">
                      Number of Activities (Desc.)
                    </option>
                  </Form.Select>
                </FloatingLabel>
              </Col>
            </Row>
            <Row>
              <Col>
                <Button
                  variant="secondary"
                  style={{
                    width: "170px",
                    marginLeft: "10px",
                    marginBottom: "10px",
                  }}
                  onClick={filter}
                >
                  Filter & Sort
                </Button>
                <Button
                  variant="secondary"
                  style={{ marginLeft: "10px", marginBottom: "10px" }}
                  onClick={reset}
                >
                  Reset
                </Button>
              </Col>
            </Row>
          </Form.Group>
        </Form>
        <Container fluid={true}>
          <Row>
            {nationalParksLoaded && nationalParks && nationalParks["data"] ? (
              nationalParks["data"].map((park, index) => {
                return (
                  <Col key={index} className="mb-4" xs={11} md={4}>
                    <Card
                      text="dark"
                      border="dark"
                      className="hover-class"
                      onClick={() => {
                        loadParkPage(park["id"]);
                      }}
                    >
                      <Card.Img
                        variant="top"
                        src={park["attributes"]["image"]}
                      />
                      <Card.Body>
                        <Card.Title className="Title mt-4">
                          <strong>
                            {searchHighlight(park["attributes"]["name"])}
                          </strong>
                        </Card.Title>
                        <Card.Subtitle className="text-muted Title mb-4">
                          {park["attributes"]["states"]}
                        </Card.Subtitle>
                        <ListGroup variant="list-group-flush">
                          <ListGroup.Item>
                            <strong>Park Code:</strong>{" "}
                            {searchHighlight(park["attributes"]["parkCode"])}
                          </ListGroup.Item>
                          <ListGroup.Item>
                            <strong>Designation:</strong>{" "}
                            {searchHighlight(park["attributes"]["designation"])}
                          </ListGroup.Item>
                          <ListGroup.Item>
                            <strong>Max Entry Fee:</strong>{" "}
                            {
                              <div>
                                {"$" +
                                  String(
                                    Number(
                                      park["attributes"]["max_fee"]
                                    ).toFixed(2)
                                  )}
                              </div>
                            }
                          </ListGroup.Item>
                          <ListGroup.Item>
                            <strong>Number of Activities:</strong>{" "}
                            {park["attributes"]["activities_cnt"]}
                          </ListGroup.Item>
                        </ListGroup>
                      </Card.Body>
                    </Card>
                  </Col>
                );
              })
            ) : (
              <tr>
                <td>
                  <h2>
                    <FontAwesomeIcon icon={faSpinner} spin />
                  </h2>
                </td>
              </tr>
            )}
          </Row>
        </Container>
        {pagination()}
      </Container>
    </div>
  );
};

NationalParksHome.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object,
};

export default NationalParksHome;
