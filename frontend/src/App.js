import React, { useState } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import NationalParksHome from "./components/NationalParksSplash";
import NationalPark from "./components/NationalPark";
import TrailsPage from "./components/TrailsPage";
import Trail from "./components/Trail";
import LodgesPage from "./components/LodgesPage.jsx";
import Lodges from "./components/Lodges.jsx";
import SearchPage from "./components/SearchPage";
import Visualizations from "./components/Visualizations";

import Home from "./views/Main/Home";
import About from "./views/About/About";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink,
} from "react-router-dom";
import { Nav, Navbar, Container } from "react-bootstrap";

{
  /* Source: https://react-bootstrap.github.io/components/navbar/ */
  /* Collapse on select source: https://stackoverflow.com/questions/32452695/react-bootstrap-how-to-collapse-menu-when-item-is-selected */
}

function App() {
  const [expanded, setExpanded] = useState(false);
  return (
    <Router>
      <div>
        <Navbar
          collapseOnSelect
          expand="lg"
          expanded={expanded}
          bg="dark"
          variant="dark"
        >
          <Container>
            <Navbar.Brand href="/">TrailMix</Navbar.Brand>
            <Navbar.Toggle
              aria-controls="responsive-navbar-nav"
              onClick={() => setExpanded(expanded ? false : "expanded")}
            />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="me-auto">
                <NavLink
                  as={Link}
                  exact
                  to="/"
                  className="nav-link"
                  activeClassName="nav-link active"
                  onClick={() => setExpanded(false)}
                >
                  Home
                </NavLink>
                <NavLink
                  as={Link}
                  to="/about"
                  className="nav-link"
                  activeClassName="nav-link active"
                  onClick={() => setExpanded(false)}
                >
                  About
                </NavLink>
                <NavLink
                  as={Link}
                  to="/search"
                  className="nav-link"
                  activeClassName="nav-link active"
                  onClick={() => setExpanded(false)}
                >
                  Search
                </NavLink>
                <NavLink
                  as={Link}
                  to="/parks"
                  className="nav-link"
                  activeClassName="nav-link active"
                  onClick={() => setExpanded(false)}
                >
                  National Parks
                </NavLink>
                <NavLink
                  as={Link}
                  to="/trails"
                  className="nav-link"
                  activeClassName="nav-link active"
                  onClick={() => setExpanded(false)}
                >
                  Trails
                </NavLink>
                <NavLink
                  as={Link}
                  to="/lodges"
                  className="nav-link"
                  activeClassName="nav-link active"
                  onClick={() => setExpanded(false)}
                >
                  Lodging
                </NavLink>
                <NavLink
                  as={Link}
                  to="/visualizations"
                  className="nav-link"
                  activeClassName="nav-link active"
                  onClick={() => setExpanded(false)}
                >
                  Visualizations
                </NavLink>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>

        <Switch>
          <Route path="/parks/view/:id" component={NationalPark} />
          <Route path="/trails/view/:name" component={Trail} />
          <Route exact path="/trails" component={TrailsPage} />
          <Route exact path="/parks" component={NationalParksHome} />
          <Route exact path="/search" component={SearchPage} />
          <Route exact path="/" component={Home} />
          <Route exact path="/lodges/view/:id" component={LodgesPage} />
          <Route exact path="/lodges" component={Lodges} />
          <Route exact path="/about" component={About} />
          <Route exact path="/visualizations" component={Visualizations} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
