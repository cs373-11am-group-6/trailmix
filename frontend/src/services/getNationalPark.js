async function getNationalPark(id) {
  let response = await fetch(`https://api.trailmixapp.me/park/${id}`, {
    headers: {
      "Content-Type": "application/vnd.api+json",
      Accept: "application/vnd.api+json",
    },
  });
  let data = response.json();
  //console.log(data);
  return data;
}

export default getNationalPark;
