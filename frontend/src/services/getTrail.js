async function getTrail(name) {
  let response = await fetch(`https://api.trailmixapp.me/trail/${name}`, {
    headers: {
      "Content-Type": "application/vnd.api+json",
      Accept: "application/vnd.api+json",
    },
  });
  let data = response.json();
  console.log(data);
  return data;
}

export default getTrail;
