async function getTrailsLodging(id) {
  return fetch(`https://api.trailmixapp.me/trails/${id}`, {
    headers: {
      "Content-Type": "application/vnd.api+json",
      Accept: "application/vnd.api+json",
    },
  })
    .then((response) => response.json())
    .then((data) => {
      //console.log("Success:", data);
      return data;
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

export default getTrailsLodging;
