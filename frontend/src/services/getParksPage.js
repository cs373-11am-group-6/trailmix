async function getParksPage(id) {
  return fetch(`https://api.trailmixapp.me/lodging/${id}/park`, {
    headers: { Accept: "application/vnd.api+json" },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log("Success:", data);
      return data;
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

export default getParksPage;
