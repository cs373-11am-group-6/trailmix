import React, { useState, useEffect } from "react";
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
  ResponsiveContainer,
} from "recharts";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const getNutritionInfo = async () => {
  let nutritionInfo = [];
  let dataList = [];
  let dataLog = [];
  let pageNum = 0;
  do {
    nutritionInfo = await fetch(
      `https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/nutritionalInfo?page=${pageNum}`
    );
    nutritionInfo = await nutritionInfo.json();

    dataLog = JSON.parse(nutritionInfo);

    dataLog.data.forEach((item) => {
      dataList = [
        ...dataList,
        {
          name: item.name,
          calories: item.nutrient_energy,
          protein: item.nutrient_protein,
        },
      ];
    });

    pageNum += 1;
  } while (dataLog.data.length === 10);
  return dataList;
};

const NutripalScatterChart = () => {
  const [nutrients, setNutrients] = useState([]);
  const [graphLoaded, setGraphLoaded] = useState(false);

  useEffect(() => {
    const fetchInfo = async () => {
      if (nutrients === undefined || nutrients.length === 0) {
        const info = await getNutritionInfo();

        setNutrients(info);
      }
    };
    fetchInfo();
    setGraphLoaded(true);
  }, [nutrients]);

  return (
    <div style={{ textAlign: "center" }}>
      <h1>Calories vs. Protein</h1>
      <h5>Scatter Correlation of Protein and Calories</h5>
      <ResponsiveContainer width="98%" height={500}>
        {graphLoaded && nutrients.length > 0 ? (
          <ScatterChart>
            <CartesianGrid />
            <XAxis
              type="number"
              dataKey="calories"
              name="calories"
              unit="cal"
            />
            <YAxis type="number" dataKey="protein" name="protein" unit="g" />
            <Tooltip cursor={{ strokeDasharray: "3 3" }} />
            <Scatter data={nutrients} fill="#95e3d3" />
          </ScatterChart>
        ) : (
          <div>
            <FontAwesomeIcon icon={faSpinner} size="2x" spin />
          </div>
        )}
      </ResponsiveContainer>
    </div>
  );
};

export default NutripalScatterChart;
