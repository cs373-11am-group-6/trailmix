import React, { useState, useEffect } from "react";
import { PieChart, Pie, Tooltip, ResponsiveContainer } from "recharts";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const getRecipeInfo = async () => {
  let recipeInfo = [];
  let dataList = [];
  let dataLog = [];
  let pageNum = 0;
  do {
    recipeInfo = await fetch(
      `https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/recipes?page=${pageNum}`
    );
    recipeInfo = await recipeInfo.json();

    dataLog = JSON.parse(recipeInfo);

    dataLog.data.forEach((item) => {
      dataList = [...dataList, item.category];
    });

    pageNum += 1;
  } while (dataLog.data.length === 10);
  return dataList;
};

const NutripalRecipeCategoryChart = () => {
  const [categories, setCategories] = useState([]);
  const [graphLoaded, setGraphLoaded] = useState(false);

  useEffect(() => {
    const fetchInfo = async () => {
      if (categories === undefined || categories.length === 0) {
        const info = await getRecipeInfo();

        var counter = {};

        info.forEach((category) => {
          if (category in counter) {
            counter[category] += 1;
          } else {
            counter[category] = 1;
          }
        });

        const data = [];

        Object.keys(counter).forEach((key) => {
          data.push({ name: key, instances: counter[key] });
        });

        setCategories(data);
      }
    };
    fetchInfo();
    setGraphLoaded(true);
  }, [categories]);

  return (
    <div style={{ textAlign: "center" }}>
      <h1>Recipe Data</h1>
      <h5>Recipe Categories</h5>
      <ResponsiveContainer width="100%" height={500}>
        {graphLoaded && categories.length > 0 ? (
          <PieChart>
            <Pie
              dataKey="instances"
              data={categories}
              cx="50%"
              cy="50%"
              outerRadius={200}
              fill="#95e3d3"
              label={(entry) => {
                return entry.name;
              }}
            />
            <Tooltip />
          </PieChart>
        ) : (
          <div>
            <FontAwesomeIcon icon={faSpinner} size="2x" spin />
          </div>
        )}
      </ResponsiveContainer>
    </div>
  );
};

export default NutripalRecipeCategoryChart;
