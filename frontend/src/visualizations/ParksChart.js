import React, { useEffect, useState } from "react";
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  Label,
} from "recharts";
import getNationalParks from "../services/getNationalParks";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const ParksChart = () => {
  const [parks, setParks] = useState([]);
  const [parksLoaded, setParksLoaded] = useState(false);
  const [scatterPlotData, setScatterPlotData] = useState([]);
  const parksPerPage = 500;

  useEffect(() => {
    getAllParks();
  }, []);

  const getAllParks = async () => {
    let promises = [];

    let parks = [];

    promises.push(
      getNationalParks(parksPerPage, 1)
        .catch((error) => {
          console.error("Error:", error);
        })
        .then((response) => {
          response["data"].forEach((park) => {
            var parkAttribute = park["attributes"];
            parks.push(parkAttribute);
          });
          return parks;
        })
    );

    Promise.all(promises).then((values) => {
      let allParks = [];
      values.forEach((value) => {
        value.forEach((park) => {
          allParks.push(park);
        });
      });

      allParks.forEach((park) => {
        let fee = park["max_fee"];
        let num_activities = park["activities_cnt"];

        let dataPoint = {
          x: num_activities,
          y: fee,
        };
        scatterPlotData.push(dataPoint);
      });

      setScatterPlotData(scatterPlotData);

      setParks(allParks);
      setParksLoaded(true);
    });
  };

  const parkScatterPlotComponent = () => {
    if (parksLoaded) {
      return (
        <ResponsiveContainer width="100%" height={500}>
          <ScatterChart
            margin={{
              top: 5,
              right: 30,
              bottom: 20,
              left: 40,
            }}
          >
            <CartesianGrid />
            <XAxis
              type="number"
              dataKey="x"
              name="Number of Activities"
              unit=""
            >
              <Label value="Number of Activities" dy={20} />
            </XAxis>
            <YAxis
              type="number"
              dataKey="y"
              name="Max Fee"
              tickFormatter={(value) => "$" + value}
            >
              <Label value="Maximum Fee" angle={-90} dx={-40} />
            </YAxis>
            <Tooltip cursor={{ strokeDasharray: "3 3" }} />
            <Scatter
              name="Number of activities vs Max Fee of a Park"
              data={scatterPlotData}
              fill="#8884d8"
            />
          </ScatterChart>
        </ResponsiveContainer>
      );
    }
  };

  return (
    <div style={{ textAlign: "center" }}>
      <h1>Parks Data</h1>
      <h5>Number of Activities vs Max Fee of a Park</h5>
      {parksLoaded && parks.length > 0 && (
        <div>{parkScatterPlotComponent()}</div>
      )}
      {(!parksLoaded || parks.length == 0) && (
        <div>
          <FontAwesomeIcon icon={faSpinner} size="2x" spin />
        </div>
      )}
    </div>
  );
};

export default ParksChart;
