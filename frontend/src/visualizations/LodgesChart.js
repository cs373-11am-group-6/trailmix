import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { React, useEffect, useState } from "react";
import { ResponsiveContainer } from "recharts";
import getLodges from "../services/getLodges";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import BubbleChart from "@weknow/react-bubble-chart-d3";

const LodgesChart = () => {
  const [lodges, setLodges] = useState([]);
  const [lodgesLoaded, setLodgesLoaded] = useState(false);
  const [bubbleChartData, setBubbleChartData] = useState([]);
  const lodgesPerPage = 500;
  const heightPadding = "90%";

  useEffect(() => {
    getAllLodges();
  }, []);

  const getAllLodges = async () => {
    let promises = [];

    for (let i = 1; i <= 3; i++) {
      let lodges = [];

      promises.push(
        getLodges(lodgesPerPage, i)
          .catch((error) => {
            console.error("Error:", error);
          })
          .then((response) => {
            response["data"].forEach((lodge) => {
              var lodgeAttributes = lodge["attributes"];
              lodges.push(lodgeAttributes);
            });
            return lodges;
          })
      );
    }

    Promise.all(promises).then((values) => {
      let allLodges = [];
      values.forEach((value) => {
        value.forEach((lodge) => {
          allLodges.push(lodge);
        });
      });

      allLodges.forEach((lodge) => {
        let found = {};
        let state = lodge["state"];
        if (state != "" && state.length == 2) {
          found = bubbleChartData.find((e) => e.label == state);
          if (found) {
            let totalLodging = found.value + 1;
            bubbleChartData[bubbleChartData.indexOf(found)].value =
              totalLodging;
          } else {
            let color = "#";
            let statePoint = {
              label: state,
              value: 1,
              color: color.concat(
                Math.floor(Math.random() * 16777215).toString(16)
              ),
            };
            bubbleChartData.push(statePoint);
          }
        }
      });

      setBubbleChartData(bubbleChartData);

      setLodges(allLodges);
      setLodgesLoaded(true);
    });
  };

  const lodgeChartComponent = () => {
    if (lodgesLoaded) {
      return (
        <ResponsiveContainer height={heightPadding}>
          <BubbleChart
            graph={{
              zoom: 0.7,
              offsetX: 0.15,
              offsetY: 0.02,
            }}
            showLegend={false}
            valueFont={{
              family: "Arial",
              size: 12,
              color: "#fff",
              weight: "bold",
            }}
            labelFont={{
              family: "Arial",
              size: 16,
              color: "#fff",
              weight: "bold",
            }}
            data={bubbleChartData}
          />
        </ResponsiveContainer>
      );
    }
  };
  // styling adapted from https://www.w3schools.com/howto/howto_css_aspect_ratio.asp
  return (
    <div
      className="lodgebox"
      style={{
        textAlign: "center",
        width: "100%",
        paddingTop: lodgesLoaded ? heightPadding : "120px",
      }}
    >
      <div
        style={{
          position: "absolute",
          top: "2em",
          left: "0",
          bottom: "0",
          right: "0",
          overflow: "auto",
        }}
      >
        <h1>Lodging Data</h1>
        <h5>Lodging Available By State</h5>
        {lodgesLoaded && lodges.length > 0 && lodgeChartComponent()}
        {(!lodgesLoaded || lodges.length == 0) && (
          <div>
            <FontAwesomeIcon icon={faSpinner} size="2x" spin />
          </div>
        )}
      </div>
    </div>
  );
};

export default LodgesChart;
