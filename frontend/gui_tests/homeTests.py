import time
import sys
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException


DRIVER = "./frontend/gui_tests/chromedriver.exe"
PAGE = "/"


class homeTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(DRIVER, options=options)
        cls.driver.get(URL)
        time.sleep(5)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testParksCard(self):
        self.driver.find_elements_by_class_name("card-body")[0].click()
        pageTitle = self.driver.find_element_by_tag_name("h1")
        assert pageTitle.text == "National Parks"

        self.driver.back()
        assert self.driver.current_url == URL

    def testTrailsCard(self):
        self.driver.find_elements_by_class_name("card-body")[1].click()
        pageTitle = self.driver.find_element_by_tag_name("h1")
        assert pageTitle.text == "Hike & Bike Trails"

        self.driver.back()
        assert self.driver.current_url == URL

    def testLodgesCard(self):
        self.driver.find_elements_by_class_name("card-body")[2].click()
        pageTitle = self.driver.find_element_by_tag_name("h1")
        assert pageTitle.text == "Lodging"

        self.driver.back()
        assert self.driver.current_url == URL

    def testPrezButton(self):
        self.driver.find_element_by_class_name("btn-primary").click()
        self.driver.find_element_by_class_name("video-responsive").click()
        assert (
            self.driver.find_element_by_class_name("modal-title").text
            == "Presentation Video"
        )
        self.driver.find_element_by_class_name("btn-secondary").click()


if __name__ == "__main__":
    DRIVER = sys.argv[1]
    URL = sys.argv[2] + PAGE
    unittest.main(argv=["first-arg-is-ignored"])
