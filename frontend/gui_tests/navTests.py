import sys
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


DRIVER = "./frontend/gui_tests/chromedriver.exe"
PAGE = "/"


class navTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--window-size=1920,1080")
        cls.driver = webdriver.Chrome(DRIVER, options=options)
        cls.driver.get(URL)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testAboutNav(self):
        nav = self.driver.find_element_by_class_name("navbar-nav")
        nav.find_elements_by_class_name("nav-link")[1].click()
        pageTitle = self.driver.find_element_by_tag_name("h1")
        assert pageTitle.text == "About Us"

    def testParkNav(self):
        nav = self.driver.find_element_by_class_name("navbar-nav")
        nav.find_elements_by_class_name("nav-link")[3].click()
        pageTitle = self.driver.find_element_by_tag_name("h1")
        assert pageTitle.text == "National Parks"

    def testTrailsNav(self):
        nav = self.driver.find_element_by_class_name("navbar-nav")
        nav.find_elements_by_class_name("nav-link")[4].click()
        pageTitle = self.driver.find_element_by_tag_name("h1")
        assert pageTitle.text == "Hike & Bike Trails"

    def testLodgeNav(self):
        nav = self.driver.find_element_by_class_name("navbar-nav")
        nav.find_elements_by_class_name("nav-link")[5].click()
        pageTitle = self.driver.find_element_by_tag_name("h1")
        assert pageTitle.text == "Lodging"

    def testHomeNav(self):
        nav = self.driver.find_element_by_class_name("navbar-nav")
        nav.find_elements_by_class_name("nav-link")[0].click()
        pageTitle = self.driver.find_element_by_tag_name("h1")
        assert pageTitle.text == "TrailMix"

    def testLogoNav(self):
        nav = self.driver.find_element_by_class_name("navbar-nav")
        nav.find_elements_by_class_name("nav-link")[3].click()
        pageTitle = self.driver.find_element_by_tag_name("h1")
        assert pageTitle.text == "National Parks"

        self.driver.find_element_by_class_name("navbar-brand").click()
        pageTitle = self.driver.find_element_by_tag_name("h1")
        assert pageTitle.text == "TrailMix"


if __name__ == "__main__":
    DRIVER = sys.argv[1]
    URL = sys.argv[2] + PAGE
    unittest.main(argv=["first-arg-is-ignored"])
