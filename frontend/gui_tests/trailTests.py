import time
import sys
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select


DRIVER = "./frontend/gui_tests/chromedriver.exe"
PAGE = "/trails"


class trailTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(DRIVER, options=options)
        cls.driver.get(URL)
        time.sleep(5)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testTrailCards(self):
        self.driver.find_element_by_class_name("card").click()
        assert self.driver.current_url != URL
        self.driver.back()
        assert self.driver.current_url == URL
        time.sleep(5)

    def testImageButton(self):
        self.driver.find_element_by_class_name("card").click()
        time.sleep(2)
        button = self.driver.find_element_by_class_name("btn")
        open = button.find_elements_by_xpath(
            '../div[@class = "accordion-collapse collapse show"]'
        )
        closed = button.find_elements_by_xpath(
            '../div[@class = "accordion-collapse collapse"]'
        )
        assert len(open) == 1
        assert len(closed) == 0
        button.click()
        time.sleep(1)
        open = button.find_elements_by_xpath(
            '../div[@class = "accordion-collapse collapse show"]'
        )
        closed = button.find_elements_by_xpath(
            '../div[@class = "accordion-collapse collapse"]'
        )
        assert len(open) == 0
        assert len(closed) == 1
        self.driver.back()
        assert self.driver.current_url == URL
        time.sleep(4)

    def testSorting(self):
        sort_select = Select(
            self.driver.find_element_by_xpath(
                '//*[@id="root"]/div/div/div/form/div[2]/div[1]/div[4]/div/select'
            )
        )
        sort_select.select_by_value("-name")
        self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div/div/form/div[2]/div[2]/div/button[1]'
        ).click()
        time.sleep(4)
        trail_name = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div/div/div[1]/div/div[1]/div/div/div[1]/strong/div'
        )
        assert trail_name.text == "Yucca Canyon Trailhead"


if __name__ == "__main__":
    DRIVER = sys.argv[1]
    URL = sys.argv[2] + PAGE
    unittest.main(argv=["first-arg-is-ignored"])
