import time
import sys
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


DRIVER = "./frontend/gui_tests/chromedriver.exe"
PAGE = "/visualizations"


class vizTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(DRIVER, options=options)
        cls.driver.get(URL)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testVizTabs(self):
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[1]/div[1]/a"
        ).click()
        tmp = self.driver.find_element_by_xpath("/html/body/div/div/div/div/h1")
        assert tmp.text == "MyNutriPal Visualizations"
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[1]/div[2]/a"
        ).click()
        tmp = self.driver.find_element_by_xpath("/html/body/div/div/div/div/h1")
        assert tmp.text == "TrailMix Visualizations"

    def testVisualizations(self):
        time.sleep(10)
        assert self.driver.find_elements_by_class_name("recharts-cartesian-axis")[
            0
        ].is_displayed()
        assert self.driver.find_elements_by_css_selector("#ca")[0].is_displayed()


if __name__ == "__main__":
    DRIVER = sys.argv[1]
    URL = sys.argv[2] + PAGE
    unittest.main(argv=["first-arg-is-ignored"])
