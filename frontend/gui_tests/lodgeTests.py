import time
import sys
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


DRIVER = "./frontend/gui_tests/chromedriver.exe"
URL = "https://dev.trailmixapp.me/lodges"


class homeTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(DRIVER, options=options)
        cls.driver.get(URL)
        time.sleep(5)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testTableLinks(self):
        self.driver.find_elements_by_xpath("//table/tbody/tr")[0].click()
        assert self.driver.current_url != URL
        self.driver.back()
        assert self.driver.current_url == URL

    def testImageButton(self):
        self.driver.find_element_by_tag_name("td").click()
        time.sleep(2)
        button = self.driver.find_element_by_class_name("btn")
        open = button.find_elements_by_xpath(
            '../div[@class = "accordion-collapse collapse show"]'
        )
        closed = button.find_elements_by_xpath(
            '../div[@class = "accordion-collapse collapse"]'
        )
        assert len(open) == 1
        assert len(closed) == 0
        button.click()
        time.sleep(1)
        open = button.find_elements_by_xpath(
            '../div[@class = "accordion-collapse collapse show"]'
        )
        closed = button.find_elements_by_xpath(
            '../div[@class = "accordion-collapse collapse"]'
        )
        assert len(open) == 0
        assert len(closed) == 1
        self.driver.back()
        assert self.driver.current_url == URL
        time.sleep(4)


if __name__ == "__main__":
    DRIVER = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
