# TrailMix
Welcome to the GitLab repository for TrailMix!<br>
Our Makefile contains a helpful list of commands. Simply run `make` or `make help`.<br>

## Group Members

| Name           | UT EID  | GitLab ID        |
| -------------- | ------- | ---------------- |
| Tori Denney    | vsd367  | @toridenney      |
| John Jin       | jj36958 | @johnjin0000     |
| Orion Reynolds | or4246  | @orionreynolds   |
| Albert Trevino | act2534 | @alberttrevino19 |
| Philip Zeng    | pdz68   | @zeng-philip     |

The project leader for all phases was Orion (@orionreynolds).

## Links

Our website (prod): https://www.trailmixapp.me<br>
Dev website: https://dev.trailmixapp.me<br>
API endpoint: https://api.trailmixapp.me/<br>
Postman: https://documenter.getpostman.com/view/17711210/UUy4cR54<br>
Presentation: https://www.youtube.com/watch?v=MD9cOMNVGnk<br>
GitLab Pipelines: https://gitlab.com/cs373-11am-group-6/trailmix/-/pipelines

## Git SHA

Phase 1: 70601fd3e4c256b11dae88ca10ac825ef91c1fcf<br>
Phase 2: ece8ec91702713ab0847d457910ddaca8b0c3451<br>
Phase 3: 633c9729da62092384caf6e2fc856feac3713b2c<br>
Phase 4: 31669699dbaba9d357e49692924aad5c1300d6a6

## Completion Time

### Phase 4

| Name           | Estimated (hrs.) | Actual (hrs.) |
| -------------- | ---------------- | ------------- |
| Tori Denney    | 10               | 12            |
| John Jin       | 5                | 4             |
| Orion Reynolds | 20               | 22            |
| Albert Trevino | 10               | 8             |
| Philip Zeng    | 10               | 10            |

### Phase 3

| Name           | Estimated (hrs.) | Actual (hrs.) |
| -------------- | ---------------- | ------------- |
| Tori Denney    | 20               | 19            |
| John Jin       | 15               | 20            |
| Orion Reynolds | 24               | 25            |
| Albert Trevino | 25               | 20            |
| Philip Zeng    | 20               | 22            |

### Phase 2

| Name           | Estimated (hrs.) | Actual (hrs.) |
| -------------- | ---------------- | ------------- |
| Tori Denney    | 20               | 18            |
| John Jin       | 30               | 22            |
| Orion Reynolds | 55               | 75            |
| Albert Trevino | 35               | 25            |
| Philip Zeng    | 30               | 40            |

### Phase 1

| Name           | Estimated (hrs.) | Actual (hrs.) |
| -------------- | ---------------- | ------------- |
| Tori Denney    | 10               | 10            |
| John Jin       | 10               | 18            |
| Orion Reynolds | 12               | 20            |
| Albert Trevino | 8                | 12            |
| Philip Zeng    | 10               | 20            |

## Comments

README formatting, backend structure, Selenium code, Chrome installation, and database reset code adapted from [TexasVotes](https://gitlab.com/forbesye/fitsbits/-/blob/master/README.md).<br>
The server.py Flask code was adapted from the [Flask-Restless-NG Quickstart](https://flask-restless-ng.readthedocs.io/en/latest/quickstart.html).<br>
CD rules in the GitLab Pipeline configuration incorporate code from [this Medium post](https://medium.com/@agungdarmanto/how-to-deploy-from-gitlab-to-aws-elastic-beanstalk-eb-87e7c11cbd8c) for AWS, and [this one](https://medium.com/google-cloud/automatically-deploy-to-google-app-engine-with-gitlab-ci-d1c7237cbe11) for Google Cloud Platform.
