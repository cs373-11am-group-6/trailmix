# Adapted from Burninup's Dockerfile, as well as Dockerfile Best Practices doc
# https://gitlab.com/caitlinlien/cs373-sustainability/-/blob/master/Dockerfile
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/

FROM nikolaik/python-nodejs:python3.10-nodejs16
SHELL ["bash", "-c"]
COPY . /app
WORKDIR /app

RUN apt-get update
RUN apt-get install vim make -y

RUN make install-chrm
RUN make install
RUN make build-fe

ENV FLASK_APP="backend/server.py"
EXPOSE 5000
CMD serve -s frontend/build
