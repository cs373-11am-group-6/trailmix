.DEFAULT_GOAL := help
SHELL         := bash
IMAGE         := registry.gitlab.com/cs373-11am-group-6/trailmix:latest
DRIVER 		  := frontend/gui_tests/chromedriver_linux
URL			  := https://dev.trailmixapp.me

help:
	@echo "Current make rules:"
	@echo "-------------------------------------------------------------"
	@echo "push:            commit & push all changes (must specify MSG=\"\")"
	@echo "status:          get Git status"
	@echo "clean:           remove temporary files and build artifacts"
	@echo "all:             run format, linter, then test"
	@echo "docker:          run Docker image for development purposes"
	@echo "  docker-be:       run Docker image for backend app"
	@echo "  docker-fe:       run Docker image for frontend app"
	@echo "  fix-perms:       fix permissions issues caused by docker"
	@echo "install:         install frontend and backend dependencies only"
	@echo "  install-be:    install python dependencies"
	@echo "  install-fe:    install yarn packages"
	@echo "  install-chrm:  install chrome for selenium testing (linux only, use sudo)"
	@echo "format:          make code prettier"
	@echo "  format-be:       run black"
	@echo "  format-fe:       run prettier"
	@echo "lint:            run linters"
	@echo "  lint-be:         run pylint and mypy"
	@echo "  lint-fe:         run eslint"
	@echo "test:            run all tests, except Postman"
	@echo "  test-be:         run backend tests"
	@echo "  test-fe:         run frontend tests"
	@echo "  test-gui:        run Selenium tests"
	@echo "  test-api:        run Postman tests (note: uses Docker)"
	@echo "run-be:          run the backend server in development mode"
	@echo "run-fe:          check dependencies and run React app in development mode"
	@echo "build-fe:        check dependencies and build React app for production"

install-chrm:
	apt-get install -y gconf-service libasound2 libatk1.0-0 libcairo2 libcups2 libfontconfig1 libgdk-pixbuf2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libxss1 fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils libgbm1 libegl-mesa0
	wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
	dpkg -i google-chrome-stable_current_amd64.deb
	apt-get -fy install
	rm -f google-chrome-stable_current_amd64.deb

install-fe:
	cd frontend && yarn install
	yarn global add serve

install-be:
	pip install --upgrade pip
	pip install -r backend/requirements.txt

install: install-be install-fe

# pull docker image; called by all docker rules
docker-pull:
	docker pull $(IMAGE)

# run docker with virtual mapping, for development purposes
# note that any files made in the container will belong to root
# you can fix this using the fix-perms rule
docker: docker-pull
	docker run --rm -it -p 3000:3000 -p 5000:5000 -v "$(PWD)":/usr/trailmix -w /usr/trailmix $(IMAGE) bash

# fix permissions from running docker
fix-perms:
	sudo chown -R ${USER} .

# run prod-level frontend on the latest docker image
docker-fe: docker-pull
	docker run --rm -it -p 3000:3000 $(IMAGE)

# run prod-level backend on the latest docker image
docker-be: docker-pull
	@echo "NOTE: If the link displayed does not work, try localhost:5000"
	@echo "NOTE 2: This command will not work if you don't have the database keys in backend/.env"
	docker run --rm -it -p 5000:5000 --env-file ./backend/.env $(IMAGE) bash -c "cd backend && gunicorn -w 1 -b 0.0.0.0:5000 -t 15 server:app"

# get git status
status:
	@echo
	git branch
	git remote -v
	git status

clean:
	rm -f backend/.coverage
	rm -f backend/.pylintrc
	rm -f backend/*.gen
	rm -f backend/*.pyc
	rm -f backend/*.tmp
	rm -f backend/*.log
	rm -f backend/*.log.*
	rm -f *.log
	rm -f *.log.*
	rm -rf backend/__pycache__
	rm -rf backend/models/__pycache__
	rm -rf backend/.mypy_cache
	rm -rf .mypy_cache
	rm -rf __pycache__
	rm -f .coverage
	rm -f backend/.coveragerc
	rm -rf frontend/build
	rm -rf frontend/coverage

all: clean format linter test

# auto format javascript code
format-fe:
	cd frontend/ && yarn && yarn run prettier --write .

# auto format python code
format-be:
	black backend/
	black frontend/gui_tests/

# auto format everything
format: format-fe format-be

.pylintrc:
	pylint --disable=locally-disabled --reports=no --generate-rcfile > backend/$@

.coveragerc:
	@echo "[run]" > backend/.coveragerc
	@echo "omit = " >> backend/.coveragerc
	@echo "        test*" >> backend/.coveragerc
	@echo "        /usr/lib*" >> backend/.coveragerc
	@echo "        /usr/local/lib*" >> backend/.coveragerc

# run linter on python code
lint-be: .pylintrc
	pylint --load-plugins pylint_flask_sqlalchemy backend
	mypy backend

# run linter on javascript code
lint-fe:
	cd frontend/ && yarn && yarn run eslint .

# run linters
lint: lint-be lint-fe

# run backend server
run-be: format-be lint-be
	export FLASK_APP="backend/server.py" && \
	export FLASK_ENV="development" && \
	flask run --host=0.0.0.0

# run all tests
test: test-fe test-be test-gui

# run backend tests
test-be: .coveragerc
	cd backend/ && coverage run --branch -m unittest discover
	cd backend/ && coverage report -m

# run frontend tests
test-fe:
	cd frontend && yarn && CI=true yarn test --coverage

# run Selenium tests
test-gui:
	chmod +x $(DRIVER)
	python frontend/gui_tests/homeTests.py $(DRIVER) $(URL)
	python frontend/gui_tests/navTests.py $(DRIVER) $(URL)
	python frontend/gui_tests/aboutTests.py $(DRIVER) $(URL)
	python frontend/gui_tests/trailTests.py $(DRIVER) $(URL)
	python frontend/gui_tests/lodgeTests.py $(DRIVER) $(URL)
	python frontend/gui_tests/parkTests.py $(DRIVER) $(URL)
	python frontend/gui_tests/visualizationTests.py $(DRIVER) $(URL)

# run Postman tests
test-api:
	docker run --rm -t postman/newman run "https://www.getpostman.com/collections/e235cb74b8c47f45473a"

# build React app
build-fe: format-fe lint-fe
	cd frontend/ && yarn && yarn build

# run React app in dev mode
run-fe: format-fe lint-fe
	cd frontend/ && yarn start

# commit and push all changes
push:
	git pull
	make clean
	make format
	git add .
	git commit -a -m "$(MSG)"
	git push

