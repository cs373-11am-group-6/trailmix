# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = too-many-lines

from typing import Any, Dict
import unittest
import logging
import os
from unittest import mock
from flask_sqlalchemy import SQLAlchemy
from google.cloud.logging_v2.handlers.handlers import CloudLoggingHandler
from requests.exceptions import HTTPError
import populate_db
from models import Park, Trail, Lodging
import util

logger = logging.getLogger(__name__)
util.setup_logging(logger, ".", "tests", level=logging.DEBUG, num_backups=1)

# Backend unit tests
# remember to keep tests restricted in scope
# i.e. don't call the remote API, directly test libraries, etc.
# use mocking whenever needed


class TestModels(unittest.TestCase):
    def test_park_repr(self):
        park = Park(id=3, name="park")

        self.assertEqual(str(park), "<Park 3: park>")

    def test_trail_repr(self):
        trail = Trail(name="trail")
        self.assertEqual(str(trail), "<Trail: trail>")

    def test_lodging_repr(self):
        lodging = Lodging(id=7, name="lodging")
        self.assertEqual(str(lodging), "<Lodging 7: lodging>")

    def test_park_eq(self):
        park1 = Park(id=7, name="Unique Park")
        park2 = Park(id=7, name="Unique Park")
        self.assertEqual(park1, park2)
        park2 = Park(id=2, name="Free Park-ing")
        self.assertNotEqual(park1, park2)

    def test_trail_eq(self):
        trail1 = Trail(name="Speedway")
        trail2 = Trail(name="Speedway")
        self.assertEqual(trail1, trail2)
        trail2 = Trail(name="Dean Keeton")
        self.assertNotEqual(trail1, trail2)

    def test_lodging_eq(self):
        lodg1 = Lodging(id=1, name="A tent")
        lodg2 = Lodging(id=1, name="A tent")
        self.assertEqual(lodg1, lodg2)
        lodg2 = Lodging(id=3, name="Holiday Inn")
        self.assertNotEqual(lodg1, lodg2)


# Make mocked API responses easily!
def mocker(err: bool, results: Dict[str, Any] = None, unexpected: bool = False):
    rsp = mock.Mock()
    rsp.status_code = 500 if err else 200
    rsp.raise_for_status = mock.Mock(
        side_effect=mock.Mock(return_value=None)
        if not err
        else Exception()
        if unexpected
        else HTTPError()
    )
    rsp.json = mock.Mock(return_value=results)
    sess = mock.Mock()
    sess.get = lambda url, params: rsp
    return lambda: sess


# Shortcut
geocode_mocker = lambda err, res=False, lat=None, lng=None: mocker(
    err,
    {
        "status": "OK" if res else "ZERO_RESULTS",
        "results": [{"geometry": {"location": {"lat": str(lat), "lng": str(lng)}}}],
    },
)


class TestUtils(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.key = "very/legit-api1key!"
        cls.test_logger = logging.getLogger("test_utils")
        cls.test_logger.setLevel(logging.DEBUG)

    def setUp(self):
        self.dummy_park = Park()

    def test_create_app(self):
        dummy_db = SQLAlchemy()  # feel free to move to setUp() if needed
        app = util.create_app(dummy_db, self.key)
        self.assertIsNotNone(app)
        self.assertIsNotNone(app.config["SQLALCHEMY_DATABASE_URI"])
        with app.app_context():
            self.assertIsNotNone(dummy_db.get_app())

    @mock.patch("google.cloud.logging.Client")
    def test_setup_logging_gcp(self, MockGCPLogger):
        util.setup_logging(self.test_logger, "gcp")
        self.assertTrue(MockGCPLogger.called)
        self.assertIsInstance(self.test_logger.handlers[-1], CloudLoggingHandler)

    def test_setup_logging_file(self):
        util.setup_logging(self.test_logger, ".", "logging_test")
        self.assertIsInstance(
            self.test_logger.handlers[-1], logging.handlers.RotatingFileHandler
        )
        self.test_logger.log(logging.INFO, "test")
        self.assertTrue(os.path.isfile("./logging_test.log"))
        os.remove("./logging_test.log")

    def test_setup_logging_rollover(self):
        with open("./rollover_test.log", "w", encoding="utf-8") as file:
            file.write("a" * 10000)
        self.assertTrue(os.path.isfile("./rollover_test.log"))
        util.setup_logging(self.test_logger, ".", "rollover_test")
        self.test_logger.log(logging.WARN, "test")
        self.assertTrue(os.path.isfile("./rollover_test.log"))
        self.assertTrue(os.path.isfile("./rollover_test.log.1"))
        os.remove("./rollover_test.log")
        os.remove("./rollover_test.log.1")

    def test_setup_logging_stderr(self):
        util.setup_logging(self.test_logger, "stderr")
        self.assertIsInstance(self.test_logger.handlers[-1], logging.StreamHandler)

    @mock.patch("requests.session", side_effect=geocode_mocker(False, True, 3.0, -73.0))
    def test_get_coords(self, mock_func):
        coords = util.get_coordinates(logger, self.key, "my house")
        self.assertTrue(mock_func.called)
        self.assertTupleEqual(coords, (3.0, -73.0))

    @mock.patch("requests.session", side_effect=geocode_mocker(False, True, 37.0, 3.01))
    def test_get_coords_two_addr(self, mock_func):
        coords = util.get_coordinates(logger, self.key, "my house", "Austin, TX 78712")
        self.assertTrue(mock_func.called)
        self.assertTupleEqual(coords, (37.0, 3.01))

    @mock.patch("requests.session", side_effect=geocode_mocker(False, res=False))
    def test_get_coords_no_results(self, mock_func):
        coords = util.get_coordinates(logger, self.key, "my 4.0 gpa")
        self.assertTrue(mock_func.called)
        self.assertIsNone(coords)

    @mock.patch("requests.session", side_effect=geocode_mocker(err=True))
    def test_get_coords_err(self, mock_func):
        coords = util.get_coordinates(logger, self.key, "./laugh")
        self.assertTrue(mock_func.called)
        self.assertIsNone(coords)

    def test_bin_search_single(self):
        my_list = [-9, 1, 1, 2, 4, 6, 7, 34]
        result = util.bin_search(logger, my_list, float, 2.0)
        self.assertEqual(result, 3)
        result = util.bin_search(logger, my_list, float, 34.0)
        self.assertEqual(result, 7)

    def test_bin_search_range(self):
        my_list = [-9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 77]
        result = util.bin_search(logger, my_list, float, 1.0)
        self.assertEqual(result, 1)
        result = util.bin_search(logger, my_list, float, -3.0)
        self.assertEqual(result, 1)
        result = util.bin_search(logger, my_list, float, -float("inf"))
        self.assertEqual(result, 0)

    def test_bin_search_oob(self):
        my_list = [1, 2, 3]
        result = util.bin_search(logger, my_list, float, 4.0)
        self.assertEqual(result, -1)

    def test_coord_filter(self):
        my_lodging_list = [
            Lodging(lat=4, long=2),
            Lodging(lat=4, long=1),
            Lodging(lat=44, long=1),
            Lodging(lat=-3, long=33),
            Lodging(lat=3, long=2.1),
        ]
        result = util.coord_filter(logger, my_lodging_list, (1, 2.5), (4, 4))
        self.assertListEqual(result, [Lodging(lat=4, long=2), Lodging(lat=4, long=1)])

    def test_skippable_coord_filter(self):
        my_lodging_list = [
            Lodging(lat=44, long=1),
            Lodging(lat=-3, long=33),
            Lodging(lat=3, long=2.1),
        ]

        result = util.coord_filter(logger, my_lodging_list, (5, 8), (1, 100))
        self.assertListEqual(result, [])

    def test_addr_parser(self):
        addr = {
            "line1": "201 E 21st St",
            "line2": "",
            "line3": "",
            "city": "Austin",
            "stateCode": "TX",
            "postalCode": "78705",
        }
        result = util.parse_address(logger, addr)
        self.assertEqual(result, "201 E 21st St, Austin, TX 78705")

    def test_addr_parser_full(self):
        addr = {
            "line1": "201 E 21st St",
            "line2": "Unit 123",
            "line3": "ATTN: FakeStudent",
            "city": "Austin",
            "stateCode": "TX",
            "postalCode": "78705",
        }
        result = util.parse_address(logger, addr)
        self.assertEqual(
            result, "201 E 21st St, Unit 123, ATTN: FakeStudent, Austin, TX 78705"
        )

    def test_extract_bad_desc(self):
        long_desc = (
            "I love my 3.14 mile trail. It's 330 ft high and Expert difficulty"
            ", but you don't know that because this is natural text."
        )
        short_desc = "This trail is pretty long. Kinda loopy too."
        trail = {
            "longDescription": long_desc,
            "shortDescription": short_desc,
        }
        diff, dist, desc = util.extract_description(logger, trail)
        self.assertEqual(diff, "Unknown")
        self.assertEqual(dist, 0.0)
        self.assertEqual(desc, long_desc)


class TestPark(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        fee_desc_dict = {10.0: "basic entry fee", 25.0: "holiday entry fee"}
        cls.key = "very/legit-api1key!"
        cls.map_key = "map_key"
        cls.dummy_park = Park(
            id=0,
            parkCode="test",
            name="Test National Park",
            designation="National Park",
            max_fee=25.0,
            activities_cnt=3,
            states="TX",
            lat=20.0,
            long=20.0,
            image="image url",
            imageCaption="cool image",
            phys_address="1234 Test Drive, Nowhere, NW 12345",
            mail_address="1234 Test Drive, Nowhere, NW 12345",
            email="test123@gmail.com",
            phone="1234567890",
            description="test description",
            fee_description=fee_desc_dict,
            activities=["test activity 1", "test activity 2", "test activity 3"],
        )

    @mock.patch(
        "requests.session",
        side_effect=mocker(
            False,
            {
                "total": "1",
                "data": [
                    {
                        "parkCode": "test",
                        "fullName": "test national park",
                        "designation": "National Park",
                        "states": "TX",
                        "latitude": 20,
                        "longitude": 20,
                        "images": [{"url": "image url", "caption": "cool image"}],
                        "addresses": [
                            {
                                "postalCode": "12345",
                                "city": "Nowhere",
                                "stateCode": "NW",
                                "line1": "1234 Test Drive",
                                "type": "Physical",
                                "line3": "",
                                "line2": "",
                            },
                            {
                                "postalCode": "12345",
                                "city": "Nowhere",
                                "stateCode": "NW",
                                "line1": "1234 Test Drive",
                                "type": "Mailing",
                                "line3": "",
                                "line2": "",
                            },
                        ],
                        "entranceFees": [
                            {
                                "cost": "10.00",
                                "description": "basic entry fee",
                            },
                            {
                                "cost": "25.00",
                                "description": "holiday entry fee",
                            },
                        ],
                        "contacts": {
                            "phoneNumbers": [
                                {"phoneNumber": "0000000000", "type": "Fax"},
                                {"phoneNumber": "1234567890", "type": "Voice"},
                            ],
                            "emailAddresses": [{"emailAddress": "test123@gmail.com"}],
                        },
                        "description": "test description",
                        "activities": [
                            {"name": "test activity 1"},
                            {"name": "test activity 2"},
                            {"name": "test activity 3"},
                        ],
                    },
                ],
            },
        ),
    )
    def test_park_list_generator(self, mock_func):
        park_list = populate_db.get_parks(logger, self.key, self.map_key)
        self.assertListEqual([self.dummy_park], park_list)
        self.assertTrue(mock_func.called)

    @mock.patch(
        "requests.session",
        side_effect=mocker(
            False,
            {
                "total": "1",
                "data": [
                    {
                        "parkCode": "test",
                        "fullName": "test national park",
                        "designation": "National Park",
                        "states": "TX",
                        "latitude": 20,
                        "longitude": 20,
                        "images": [{"url": "image url", "caption": "cool image"}],
                        "addresses": [
                            {
                                "postalCode": "12345",
                                "city": "Nowhere",
                                "stateCode": "NW",
                                "line1": "1234 Test Drive",
                                "type": "Physical",
                                "line3": "",
                                "line2": "",
                            },
                            {
                                "postalCode": "12345",
                                "city": "Nowhere",
                                "stateCode": "NW",
                                "line1": "1234 Test Drive",
                                "type": "Mailing",
                                "line3": "",
                                "line2": "",
                            },
                        ],
                        "entranceFees": [
                            {
                                "cost": "10.00",
                                "description": "basic entry fee",
                            },
                            {
                                "cost": "25.00",
                                "description": "holiday entry fee",
                            },
                        ],
                        "contacts": {
                            "phoneNumbers": [],
                            "emailAddresses": [{"emailAddress": "test123@gmail.com"}],
                        },
                        "description": "test description",
                        "activities": [
                            {"name": "test activity 1"},
                            {"name": "test activity 2"},
                            {"name": "test activity 3"},
                        ],
                    },
                ],
            },
        ),
    )
    def test_park_list_generator_no_phoneNums(self, mock_func):
        park_list = populate_db.get_parks(logger, self.key, self.map_key)
        self.assertEqual(len(park_list), 1)
        self.assertEqual(park_list[0].phone, "")
        self.assertTrue(mock_func.called)

    @mock.patch(
        "requests.session", side_effect=mocker(False, {"total": "0", "data": []})
    )
    def test_empty_request(self, mock_func):
        park_list = populate_db.get_parks(logger, self.key, self.map_key)

        self.assertListEqual([], park_list)

        self.assertTrue(mock_func.called)

    @mock.patch("requests.session", side_effect=mocker(True))
    def test_error_response(self, mock_func):
        park_list = populate_db.get_parks(logger, self.key, self.map_key)

        self.assertListEqual([], park_list)

        self.assertTrue(mock_func.called)

    @mock.patch("requests.session", side_effect=mocker(True, unexpected=True))
    def test_unexpected_error(self, mock_func):
        self.assertRaises(
            Exception, populate_db.get_parks, logger, self.key, self.map_key
        )
        self.assertTrue(mock_func.called)


class TestLodging(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.key = "very/legit-api1key!"
        cls.dummy_lodge = Lodging(
            id=0,
            name="Albert",
            location="Austin",
            state="TX",
            lodging_type="Camping",
            image="image",
            imageCaption="fun image",
            address1="9018 Cliff Drive",
            address2="Austin, TX 78852",
            lat=1,
            long=2,
            email="alberttrevino19@gmail.com",
            phone="9566937722",
            fee_description="fee",
            reservable=True,
            activities=["Jumping Jacks", "Jumping"],
            description="desc",
        )

    @mock.patch(
        "requests.session",
        side_effect=mocker(
            False,
            {
                "RECDATA": [
                    {
                        "FacilityID": "2",
                        "FacilityName": "albert",
                        "FACILITYADDRESS": [
                            {
                                "City": "Austin",
                                "FacilityStreetAddress1": "9018 Cliff Drive",
                                "AddressStateCode": "TX",
                                "PostalCode": "78852",
                            }
                        ],
                        "FacilityTypeDescription": "Camping",
                        "MEDIA": [
                            {
                                "MediaType": "Video",
                                "URL": "video",
                                "Title": "fun video",
                            },
                            {
                                "MediaType": "Image",
                                "URL": "image",
                                "Title": "fun image",
                            },
                        ],
                        "FacilityLatitude": "1",
                        "FacilityLongitude": "2",
                        "FacilityEmail": "alberttrevino19@gmail.com",
                        "FacilityPhone": "9566937722",
                        "FacilityUseFeeDescription": "fee",
                        "Reservable": True,
                        "ACTIVITY": [
                            {"ActivityName": "JUMPING JACKS"},
                            {"ActivityName": "JUMPING"},
                        ],
                        "FacilityDescription": "desc",
                    },
                ]
            },
        ),
    )
    def test_lodge_list_generator(self, mock_func):
        dummy_parks = [Park(lat=1, long=2, id=3)]
        lodge_list = populate_db.get_lodging(logger, self.key, dummy_parks, radius=5)
        dummy_parks[0].lodging.append(self.dummy_lodge)

        self.assertEqual(self.dummy_lodge, lodge_list[0])

        self.assertTrue(mock_func.called)

    @mock.patch(
        "requests.session",
        side_effect=mocker(
            False,
            {
                "RECDATA": [
                    {
                        "FacilityID": "2",
                        "FacilityName": "albert",
                        "FACILITYADDRESS": [],
                        "FacilityTypeDescription": "Camping",
                        "MEDIA": [],
                        "FacilityLatitude": "1",
                        "FacilityLongitude": "2",
                        "FacilityEmail": "alberttrevino19@gmail.com",
                        "FacilityPhone": "9566937722",
                        "FacilityUseFeeDescription": "fee",
                        "Reservable": True,
                        "ACTIVITY": [
                            {"ActivityName": "JUMPING JACKS"},
                            {"ActivityName": "JUMPING"},
                        ],
                        "FacilityDescription": "desc",
                    },
                ]
            },
        ),
    )
    def test_lodge_list_generator_missing_info(self, mock_func):
        dummy_parks = [Park(lat=3, long=7, id=3)]
        lodge_list = populate_db.get_lodging(logger, self.key, dummy_parks, radius=5)

        self.assertEqual(len(lodge_list), 1)

        self.assertEqual(lodge_list[0].image, "")
        self.assertEqual(lodge_list[0].imageCaption, "")

        self.assertEqual(lodge_list[0].address1, "")
        self.assertEqual(lodge_list[0].address2, "")
        self.assertEqual(lodge_list[0].state, "")
        self.assertEqual(lodge_list[0].location, "")

        self.assertTrue(mock_func.called)

    @mock.patch(
        "requests.session",
        side_effect=mocker(
            False,
            {
                "RECDATA": [
                    {
                        "FacilityID": "2",
                        "FacilityName": "albert",
                        "FACILITYADDRESS": [
                            {
                                "City": "Austin",
                                "FacilityStreetAddress1": "9018 Cliff Drive",
                                "AddressStateCode": "TX",
                                "PostalCode": "78852",
                            }
                        ],
                        "FacilityTypeDescription": "Camping",
                        "MEDIA": [
                            {
                                "MediaType": "Video",
                                "URL": "video",
                                "Title": "fun video",
                            },
                            {
                                "MediaType": "Image",
                                "URL": "image",
                                "Title": "fun image",
                            },
                        ],
                        "FacilityLatitude": "1",
                        "FacilityLongitude": "2",
                        "FacilityEmail": "alberttrevino19@gmail.com",
                        "FacilityPhone": "9566937722",
                        "FacilityUseFeeDescription": "fee",
                        "Reservable": True,
                        "ACTIVITY": [
                            {"ActivityName": "JUMPING JACKS"},
                            {"ActivityName": "JUMPING"},
                        ],
                        "FacilityDescription": "desc",
                    },
                ]
            },
        ),
    )
    def test_lodge_duplicate(self, mock_func):
        dummy_parks = [Park(lat=1, long=2, id=3), Park(lat=3, long=2, id=1)]
        lodge_list = populate_db.get_lodging(logger, self.key, dummy_parks, radius=5)
        self.assertEqual(len(lodge_list), 1)
        dummy_parks[0].lodging.append(self.dummy_lodge)

        self.assertListEqual([self.dummy_lodge], lodge_list)

        self.assertTrue(mock_func.called)

    @mock.patch("requests.session", side_effect=mocker(False, {"RECDATA": []}))
    def test_empty_request(self, mock_func):
        dummy_parks = [Park(lat=1, long=2)]
        lodge_list = populate_db.get_lodging(logger, self.key, dummy_parks, radius=5)

        self.assertListEqual([], lodge_list)

        self.assertTrue(mock_func.called)

    @mock.patch("requests.session", side_effect=mocker(True))
    def test_error_response(self, mock_func):
        dummy_parks = [Park(lat=2, long=2)]
        lodge_list = populate_db.get_lodging(logger, self.key, dummy_parks, radius=5)

        self.assertListEqual([], lodge_list)

        self.assertTrue(mock_func.called)

    @mock.patch("requests.session", side_effect=mocker(True, unexpected=True))
    def test_unexpected_error(self, mock_func):
        dummy_parks = [Park(lat=2, long=2)]
        lodge_list = populate_db.get_lodging(logger, self.key, dummy_parks, radius=5)

        self.assertListEqual([], lodge_list)

        self.assertTrue(mock_func.called)


class TestTrails(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.key = "very/legit-api1key!"

    @mock.patch(
        "requests.session",
        side_effect=mocker(
            False,
            {
                "data": [
                    {
                        "location": "Papaya Trail",
                        "title": "Hike to Papaya Falls",
                        "images": [
                            {
                                "url": "https://www.supercoolwebsite.com/papaya.jpg",
                                "caption": "An image of a papaya",
                            }
                        ],
                        "relatedParks": [{"states": "TX,CA"}],
                        "latitude": "50",
                        "longitude": "-50.407",
                        "longDescription": (
                            "<ul> <li><strong>Distance:</strong> 3.4 miles "
                            "round trip</li> <li><strong>"
                            "Elevation Gain:</strong> 205 feet</li> <li><strong>Type of "
                            "Trail: </strong>Out & Back, "
                            "Moderate</li> </ul> <p>From the Fern Lake Trailhead, the hike to "
                            "The Pool is around "
                            "3.4 miles round-trip with 205 feet of elevation gain. This "
                            "trail runs along the "
                            "Big Thompson River, and offers great fishing opportunities - "
                            "just be sure to fami"
                            "liarize yourself with fishing regulations. Along the "
                            "trail you will find plenty of "
                            "shade as well as blooming wildflowers throughout spring "
                            "and summer.</p> <p><strong>"
                            "Storm Warning: </strong>Weather in Colorado can turn in "
                            "an instant! Be prepared for "
                            "changing weather conditions. Check the forecast before "
                            "heading out. Lightning can str"
                        ),
                        "activities": [{"name": "Hiking"}],
                        "topics": [{"name": "Papayas"}],
                        "arePetsPermitted": "false",
                        "doFeesApply": "false",
                        "isReservationRequired": "false",
                        "tags": ["Hiking"],
                    }
                ]
            },
        ),
    )
    def test_trail_list_generator(self, mock_func):
        dummy_parks = [Park(parkCode="papaya")]
        trail_list = populate_db.get_trails(logger, self.key, self.key, dummy_parks)
        self.assertEqual(len(trail_list), 1)
        dummy_trail = Trail(
            name="Papaya Trail",
            title="Hike to Papaya Falls",
            image="https://www.supercoolwebsite.com/papaya.jpg",
            imageCaption="An image of a papaya",
            states="TX,CA",
            lat=50.0,
            long=-50.407,
            difficulty="Unknown",
            distance=3.4,
            pets=False,
            fees=False,
            reservation=False,
            activities=["hiking"],
            topics=["papayas"],
            tags=["Hiking"],
            description=(
                "<ul> <li><strong>Distance:</strong> 3.4 miles "
                "round trip</li> <li><strong>"
                "Elevation Gain:</strong> 205 feet</li> <li><strong>Type of "
                "Trail: </strong>Out & Back, "
                "Moderate</li> </ul> <p>From the Fern Lake Trailhead, the hike to "
                "The Pool is around "
                "3.4 miles round-trip with 205 feet of elevation gain. This "
                "trail runs along the "
                "Big Thompson River, and offers great fishing opportunities - "
                "just be sure to fami"
                "liarize yourself with fishing regulations. Along the "
                "trail you will find plenty of "
                "shade as well as blooming wildflowers throughout spring "
                "and summer.</p> <p><strong>"
                "Storm Warning: </strong>Weather in Colorado can turn in "
                "an instant! Be prepared for "
                "changing weather conditions. Check the forecast before "
                "heading out. Lightning can str"
            ),
        )
        dummy_parks[0].trails.append(dummy_trail)

        self.assertEqual(
            dummy_trail,
            trail_list[0],
        )

        self.assertTrue(mock_func.called)

    @mock.patch(
        "requests.session",
        side_effect=mocker(
            False,
            {
                "data": [
                    {
                        "location": "Papaya Trail",
                        "title": "Hike to Papaya Falls",
                        "images": [
                            {
                                "url": "https://www.supercoolwebsite.com/papaya.jpg",
                                "caption": "An image of a papaya",
                            }
                        ],
                        "relatedParks": [{"states": "TX,CA"}],
                        "latitude": "",
                        "longitude": "",
                        "longDescription": (
                            "<ul> <li><strong>Distance:</strong> 3.4 miles "
                            "round trip</li> <li><strong>"
                            "Elevation Gain:</strong> 205 feet</li> <li><strong>Type of "
                            "Trail: </strong>Out & Back, "
                            "Moderate</li> </ul> <p>From the Fern Lake Trailhead, the hike to "
                            "The Pool is around "
                            "3.4 miles round-trip with 205 feet of elevation gain. This "
                            "trail runs along the "
                            "Big Thompson River, and offers great fishing opportunities - "
                            "just be sure to fami"
                            "liarize yourself with fishing regulations. Along the "
                            "trail you will find plenty of "
                            "shade as well as blooming wildflowers throughout spring "
                            "and summer.</p> <p><strong>"
                            "Storm Warning: </strong>Weather in Colorado can turn in "
                            "an instant! Be prepared for "
                            "changing weather conditions. Check the forecast before "
                            "heading out. Lightning can str"
                            "ike anywhere in Rocky. Watch for building storm clouds "
                            "and return to the trailhead im"
                            "mediately if you hear thunder. As a rule of thumb, try to "
                            "be back at the trailhead "
                            "by noon.</p>"
                        ),
                        "activities": [{"name": "Hiking"}],
                        "topics": [{"name": "Papayas"}],
                        "arePetsPermitted": "false",
                        "doFeesApply": "false",
                        "isReservationRequired": "false",
                        "tags": ["Hiking"],
                    }
                ]
            },
        ),
    )
    @mock.patch("util.get_coordinates", side_effect=[(50, -50.407)])
    def test_searching_lat_and_long(self, mock_func, mock_func2):
        dummy_parks = [Park(parkCode="papaya", name="papaya")]
        trail_list = populate_db.get_trails(logger, self.key, self.key, dummy_parks)
        self.assertEqual(len(trail_list), 1)
        dummy_trail = Trail(
            name="Papaya Trail",
            title="Hike to Papaya Falls",
            image="https://www.supercoolwebsite.com/papaya.jpg",
            imageCaption="An image of a papaya",
            states="TX,CA",
            lat=50.0,
            long=-50.407,
            difficulty="Unknown",
            distance=3.4,
            pets=False,
            fees=False,
            reservation=False,
            activities=["hiking"],
            topics=["papayas"],
            tags=["Hiking"],
            description=(
                "<ul> <li><strong>Distance:</strong> 3.4 miles "
                "round trip</li> <li><strong>"
                "Elevation Gain:</strong> 205 feet</li> <li><strong>Type of "
                "Trail: </strong>Out & Back, "
                "Moderate</li> </ul> <p>From the Fern Lake Trailhead, the hike to "
                "The Pool is around "
                "3.4 miles round-trip with 205 feet of elevation gain. This "
                "trail runs along the "
                "Big Thompson River, and offers great fishing opportunities - "
                "just be sure to fami"
                "liarize yourself with fishing regulations. Along the "
                "trail you will find plenty of "
                "shade as well as blooming wildflowers throughout spring "
                "and summer.</p> <p><strong>"
                "Storm Warning: </strong>Weather in Colorado can turn in "
                "an instant! Be prepared for "
                "changing weather conditions. Check the forecast before "
                "heading out. Lightning can str"
                "ike anywhere in Rocky. Watch for building storm clouds "
                "and return to the trailhead im"
                "mediately if you hear thunder. As a rule of thumb, try to "
                "be back at the trailhead "
                "by noon.</p>"
            ),
        )
        dummy_parks[0].trails.append(dummy_trail)

        self.assertEqual(
            dummy_trail,
            trail_list[0],
        )

        self.assertTrue(mock_func.called)
        self.assertTrue(mock_func2.called)

    @mock.patch(
        "requests.session",
        side_effect=mocker(
            False,
            {
                "data": [
                    {
                        "location": "Papaya Trail",
                        "title": "Hike to Papaya Falls",
                        "images": [
                            {
                                "url": "https://www.supercoolwebsite.com/papaya.jpg",
                                "caption": "An image of a papaya",
                            }
                        ],
                        "shortDescription": "3.4-mile loop",
                        "relatedParks": [{"states": "TX,CA"}],
                        "latitude": "",
                        "longitude": "",
                        "longDescription": (
                            "<ul> <li><strong>Difficulty:</strong> Easy "
                            "round trip</li> <li><strong>"
                            "Elevation Gain:</strong> 205 feet</li> <li><strong>Type of "
                            "Trail: </strong>Out & Back, "
                            "Moderate</li> </ul> <p>From the Fern Lake Trailhead, the hike to "
                            "The Pool is around "
                            "3.4 miles round-trip with 205 feet of elevation gain. This "
                            "trail runs along the "
                            "Big Thompson River, and offers great fishing opportunities - "
                            "just be sure to fami"
                            "liarize yourself with fishing regulations. Along the "
                            "trail you will find plenty of "
                            "shade as well as blooming wildflowers throughout spring "
                            "and summer.</p> <p><strong>"
                            "Storm Warning: </strong>Weather in Colorado can turn in "
                            "an instant! Be prepared for "
                            "changing weather conditions. Check the forecast before "
                            "heading out. Lightning can str"
                            "ike anywhere in Rocky. Watch for building storm clouds "
                            "and return to the trailhead im"
                            "mediately if you hear thunder. As a rule of thumb, try to "
                            "be back at the trailhead "
                            "by noon.</p>"
                        ),
                        "activities": [{"name": "Hiking"}],
                        "topics": [{"name": "Papayas"}],
                        "arePetsPermitted": "false",
                        "doFeesApply": "false",
                        "isReservationRequired": "false",
                        "tags": ["Hiking"],
                    }
                ]
            },
        ),
    )
    @mock.patch("util.get_coordinates", side_effect=[None])
    def test_no_lat_long_found(self, mock_func, mock_func2):
        dummy_parks = [Park(parkCode="papaya", name="papaya park", lat=0.0, long=0.0)]
        trail_list = populate_db.get_trails(logger, self.key, self.key, dummy_parks)
        self.assertEqual(len(trail_list), 1)
        dummy_trail = Trail(
            name="Papaya Trail",
            title="Hike to Papaya Falls",
            image="https://www.supercoolwebsite.com/papaya.jpg",
            imageCaption="An image of a papaya",
            states="TX,CA",
            lat=0.0,
            long=0.0,
            difficulty="Easy",
            distance=0.0,
            pets=False,
            fees=False,
            reservation=False,
            activities=["hiking"],
            topics=["papayas"],
            tags=["Hiking"],
            description=(
                "<ul> <li><strong>Difficulty:</strong> Easy "
                "round trip</li> <li><strong>"
                "Elevation Gain:</strong> 205 feet</li> <li><strong>Type of "
                "Trail: </strong>Out & Back, "
                "Moderate</li> </ul> <p>From the Fern Lake Trailhead, the hike to "
                "The Pool is around "
                "3.4 miles round-trip with 205 feet of elevation gain. This "
                "trail runs along the "
                "Big Thompson River, and offers great fishing opportunities - "
                "just be sure to fami"
                "liarize yourself with fishing regulations. Along the "
                "trail you will find plenty of "
                "shade as well as blooming wildflowers throughout spring "
                "and summer.</p> <p><strong>"
                "Storm Warning: </strong>Weather in Colorado can turn in "
                "an instant! Be prepared for "
                "changing weather conditions. Check the forecast before "
                "heading out. Lightning can str"
                "ike anywhere in Rocky. Watch for building storm clouds "
                "and return to the trailhead im"
                "mediately if you hear thunder. As a rule of thumb, try to "
                "be back at the trailhead "
                "by noon.</p>"
            ),
        )
        dummy_parks[0].trails.append(dummy_trail)

        self.assertEqual(
            dummy_trail,
            trail_list[0],
        )

        self.assertTrue(mock_func.called)
        self.assertTrue(mock_func2.called)

    @mock.patch(
        "requests.session",
        side_effect=mocker(
            False,
            {
                "data": [
                    {
                        "location": "Papaya",
                        "tags": ["Baking"],
                        "topics": [{"id": "abcdefg", "name": "Food"}],
                        "title": "Papaya",
                    }
                ]
            },
        ),
    )
    def test_no_trails_to_do(self, mock_func):
        dummy_parks = [Park(parkCode="papaya")]
        trail_list = populate_db.get_trails(logger, self.key, self.key, dummy_parks)
        self.assertEqual([], trail_list)

        self.assertTrue(mock_func.called)

    @mock.patch("requests.session", side_effect=mocker(False, {"data": []}))
    def test_empty_response(self, mock_func):
        dummy_parks = [Park(parkCode="papaya")]
        trail_list = populate_db.get_trails(logger, self.key, self.key, dummy_parks)

        self.assertListEqual([], trail_list)

        self.assertTrue(mock_func.called)

    @mock.patch("requests.session", side_effect=mocker(True))
    def test_error_response(self, mock_func):
        dummy_parks = [Park(parkCode="papaya")]
        trail_list = populate_db.get_trails(logger, self.key, self.key, dummy_parks)

        self.assertListEqual([], trail_list)

        self.assertTrue(mock_func.called)

    @mock.patch("requests.session", side_effect=mocker(True, unexpected=True))
    def test_unexpected_error(self, mock_func):
        dummy_parks = [Park(parkCode="papaya")]
        trail_list = populate_db.get_trails(logger, self.key, self.key, dummy_parks)

        self.assertListEqual([], trail_list)

        self.assertTrue(mock_func.called)


class TestLinking(unittest.TestCase):
    def test_link(self):
        lodging = [
            Lodging(lat=3, long=3),  # this will be lodging[2] after the sort
            Lodging(lat=2, long=2),
            Lodging(lat=5, long=2),
            Lodging(lat=3.1, long=3),
        ]
        trails = [
            Trail(lat=5, long=2.1),
            Trail(lat=88, long=-130),
            Trail(lat=3, long=3.1),
        ]
        populate_db.link_lt(logger, lodging, trails, 25)
        self.assertEqual(len(lodging[2].trails), 1)
        self.assertEqual(len(trails[0].lodging), 1)
        self.assertEqual(len(trails[1].lodging), 0)
        self.assertEqual(len(trails[2].lodging), 2)


if __name__ == "__main__":
    unittest.main()
