# for use with deployments that repopulate the database
# to trigger GCP to start the application
# needs AWS CLI to restart EB instance so it gives up its lock

FROM amazon/aws-cli
SHELL ["bash", "-c"]
WORKDIR /setup

# install newman
RUN curl -fsSL https://rpm.nodesource.com/setup_16.x | bash -
RUN yum install -y gcc-c++ make nodejs
RUN npm install -g newman
RUN newman --version

ENTRYPOINT ["bash", "-c"]
CMD "bash"