"""
Util functions/classes
"""
import logging
from typing import Any, Callable, Dict, List, Tuple, Union
from logging.handlers import RotatingFileHandler
import re
import google.cloud.logging
from google.cloud.logging.handlers import CloudLoggingHandler
import requests
from requests.exceptions import RequestException
from requests.sessions import HTTPAdapter
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from models import Lodging


def create_app(database: SQLAlchemy, db_key: str) -> Flask:
    """
    Create a new app context and prepare SQLAlchemy object for it
    Caller must create database object (e.g. db = SQLAlchemy())

    pre: database != None
    """
    app: Flask = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = db_key
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    database.init_app(app)
    return app


def setup_logging(
    logger: logging.Logger,
    loc: str,
    name: str = "",
    level: int = logging.INFO,
    num_backups: int = 3,
) -> None:
    """
    Setup a logger object according to the following rules:
    - if loc == "gcp", a Google Cloud Platform Cloud Logger, or
    - if loc == "stderr", a basic logger to standard output, or
    - a logger to loc/name.log that rotates and keeps up to 8 backups
    """
    handler = logging.StreamHandler()  # default: stderr
    if (
        loc == "gcp"
    ):  # source: https://googleapis.dev/python/logging/latest/stdlib-usage.html
        client = google.cloud.logging.Client()
        handler = CloudLoggingHandler(client)
        handler.setLevel(logging.DEBUG)
    elif loc != "stderr":  # to a file
        file_name = loc + ("/" if loc[-1] != "/" else "") + name + ".log"
        handler = RotatingFileHandler(
            file_name, mode="w", maxBytes=5000, backupCount=num_backups
        )
    handler.setFormatter(
        logging.Formatter("%(asctime)s:%(name)s: [%(levelname)s] %(message)s")
    )
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.setLevel(level)


def parse_address(logger: logging.Logger, addr: Any) -> str:
    """
    Turn a NPS API address json into a properly formatted string

    Return a string of format line1, line2, line3, City, STATE zip
    where empty lines are omitted
    """
    logger.debug(
        "Parsing address: line1 %spresent, line2 %spresent, line3 %spresent,"
        "city %spresent, state %spresent, zip %spresent",
        "not" if "line1" not in addr else "",
        "not" if "line2" not in addr else "",
        "not" if "line3" not in addr else "",
        "not" if "city" not in addr else "",
        "not" if "stateCode" not in addr else "",
        "not" if "postalCode" not in addr else "",
    )
    return (
        str(addr["line1"])
        + ", "
        + (str(addr["line2"]) + ", " if len(addr["line2"]) > 0 else "")
        + (str(addr["line3"]) + ", " if len(addr["line3"]) > 0 else "")
        + str(addr["city"])
        + ", "
        + str(addr["stateCode"])
        + " "
        + str(addr["postalCode"])
    )


def get_coordinates(
    logger: logging.Logger, key: str, address1: str, address2: str = None
) -> Union[Tuple[float, float], None]:
    """
    Get coordinates given an address using Google Maps API
    If address is two lines, append second line to end of first before querying

    Return (latitude, longitude) or None if no matching place found
    """
    addr = address1 + (", " + address2 if address2 is not None else "")
    parameters = {"address": addr, "key": key}
    session = requests.session()
    session.mount("https://", HTTPAdapter(max_retries=1))
    geocode = None
    try:
        logger.info(
            "Retrieving coordinates for %s, %s from Google Maps:", address1, address2
        )
        geocode = session.get(
            "https://maps.googleapis.com/maps/api/geocode/json", params=parameters
        )
        geocode.raise_for_status()  # raise error for 4XX and 5XX HTTP errors
    except RequestException:
        logger.exception(
            "   Got bad response from Geocoding API or failed to call: %d",
            geocode.status_code if geocode is not None else -1,
        )
    else:
        logger.debug("   Got good response from Geocoding API")
        geo_json = geocode.json()
        if geo_json["status"] == "OK":
            coords = geo_json["results"][0]["geometry"]["location"]
            lat, lng = float(coords["lat"]), float(coords["lng"])
            logger.info("   Retrieved coordinates: %.3f, %.3f", lat, lng)
            return (lat, lng)
    logger.warning("   No coordinates found!")
    return None


def bin_search(
    logger: logging.Logger, arr: List[Any], transform: Callable[[Any], Any], goal: Any
) -> int:
    """
    Binary search for the smallest element in the given list that is at least goal.
    Applies transform to each element in the list to get a comparable object, i.e.
    x -> transform(x), before comparing. This function must not modify the input.

    Return the index of the smallest value in arr >= goal
    """
    begin = 0
    end = len(arr) - 1
    prev_found = -1
    while begin <= end:
        mid = (begin + end) // 2
        if transform(arr[mid]) >= goal:
            prev_found = mid
            end = mid - 1
        else:  # transfor(arr[mid]) < goal
            begin = mid + 1
    logger.debug(
        "Binary search: found position %d with goal %s in list %s",
        prev_found,
        str(goal),
        arr,
    )
    return prev_found


def coord_filter(
    logger: logging.Logger,
    lodging: List[Lodging],
    lng_range: Tuple[float, float],
    lat_range: Tuple[float, float],
) -> List[Lodging]:
    """
    Filter the Lodging list into only those instances within the given range.
    The longitude range is [lng_range[0], lng_range[1]] and the latitude range
    is [lat_range[0], lat_range[1]].

    Precondition: Lodging list is sorted

    Return the filtered list
    """
    filtered: List[Lodging] = []
    logger.info("Lodging coordinate filter start:")
    logger.debug(
        "   lng_start: %.3f, lng_stop: %.3f, lat_start: %.3f, lat_stop: %.3f",
        *lng_range,
        *lat_range
    )
    logger.debug("   list: %s", lodging)
    if lodging[-1].long >= lng_range[0] and lodging[0].long <= lng_range[1]:
        range_start = bin_search(logger, lodging, lambda x: x.long, lng_range[0])
        logger.info(
            "   Got longitude range start pos %d (long = %.3f)",
            range_start,
            lodging[range_start].long,
        )
        cur_index = range_start
        while 0 <= cur_index < len(lodging) and lodging[cur_index].long <= lng_range[1]:
            if lat_range[0] <= lodging[cur_index].lat <= lat_range[1]:
                filtered.append(lodging[cur_index])
            cur_index += 1
        logger.info(
            "   Longitude range end pos %d (long = %.3f)",
            cur_index - 1,
            lodging[cur_index - 1].long,
        )
    logger.info("   Filtered to list of length %d", len(filtered))
    logger.debug("   List: %s", filtered)
    return filtered


def remove_html_tags(text):
    """
    Helper function for get_trails that removes html tags from description text.
    Original author and source:
    https://medium.com/@jorlugaqui/how-to-strip-html-tags-from-a-string-in-python-7cb81a2bbf44
    """
    clean = re.compile("<.*?>")
    return re.sub(clean, "", text)


def extract_description(logger: logging.Logger, trail: Dict[str, str]) -> tuple:
    """
    Helper function for get_trails. Tries to extract some of the relevant stats for
    the trail from the long and short descriptions. Will definitely need improving
    in the future. Will return default unknown and zero values if info can't be
    extracted.

    Returns a list of the relevant extracted data/defaults.
    """
    description = str(trail["longDescription"])

    # clean up the description of html tags
    clean_description = remove_html_tags(description)

    trail_difficulty = "Unknown"
    trail_distance = 0.0
    if "Distance:" in description:
        split_desc = clean_description.split("Distance:", maxsplit=1)[1]

        # extract distance
        dist_results: List[str] = re.findall(r"\d*\.?\d+", split_desc)
        trail_distance = float(dist_results[0]) if len(dist_results) > 0 else -1.0
        logger.debug("   Extracted distance: %.2f", trail_distance)

    if "Difficulty:" in description:
        split_desc = clean_description.split("Difficulty:", maxsplit=1)[1]

        # extract difficulty
        trail_difficulty = split_desc.split()[0]
        logger.debug("   Extracted difficulty: %s", trail_difficulty)

    return trail_difficulty, trail_distance, description
