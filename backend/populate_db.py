# pylint: disable=too-many-locals
# pylint: disable=too-many-statements
# pylint: disable=broad-except
"""
Functions to populate the database
"""
from logging import Logger
from typing import List
from math import cos, pi
import requests
from requests.adapters import HTTPAdapter
from requests.exceptions import RequestException
import util
from models import Park, Trail, Lodging


def get_parks(logging: Logger, api_key: str, maps_key: str) -> List[Park]:
    """
    Get all parks and their details from the NPS API and Google Maps API (if needed).

    Return a list of Park instances with all fields filled out except lodging & trails
    """
    # NOTE: at one point we had errors because the key was not unique
    assert api_key is not None
    assert maps_key is not None
    logging.info("Starting Parks scrape:")
    id_num = 0
    session = requests.session()
    session.mount("https://", HTTPAdapter(max_retries=5))
    request_url = "https://developer.nps.gov/api/v1/parks"

    params = {
        "api_key": str(api_key),
        "limit": str(600),
    }
    r_quest = None
    try:
        r_quest = session.get(request_url, params=params)
        r_quest.raise_for_status()
    except RequestException:
        logging.exception(
            "    Got bad response from NPS API or failed to call: %d",
            r_quest.status_code if r_quest is not None else -1,
        )
        return []
    else:
        logging.debug("   Got good response from NPS")
    # since they are the cornerstone of all data, a failure to populate
    # all parks should (and does) crash the running server
    parks = r_quest.json()
    logging.info("   Got %s parks, looping...", parks["total"])
    parks_list = []
    for park in parks["data"]:

        image = park["images"][0]["url"] if len(park["images"]) > 0 else ""
        image_caption = park["images"][0]["caption"] if len(park["images"]) > 0 else ""

        phone_num = ""
        for p_number in park["contacts"]["phoneNumbers"]:
            if p_number["type"] == "Voice":
                phone_num = p_number["phoneNumber"]
                break
        logging.debug("      Got phone number %s", phone_num)
        phys_addr, mail_addr = "", ""

        for address in park["addresses"]:

            if address["type"] == "Physical":
                phys_addr = util.parse_address(logging, address)
            else:
                mail_addr = util.parse_address(logging, address)
        logging.debug("      Got physical address %s", phys_addr)
        logging.debug("      Got mailing address %s", mail_addr)
        maximum_fee = 0.0
        fee_description_dict = {}
        for fee in park["entranceFees"]:
            maximum_fee = max(maximum_fee, float(fee["cost"]))
            fee_description_dict[float(fee["cost"])] = fee["description"]
        logging.debug(
            "      Got %d fees, max $%.2f", len(fee_description_dict), maximum_fee
        )
        activity_list = []

        for activity in park["activities"]:
            activity_list.append(activity["name"])
        logging.debug("      Got %d activities", len(activity_list))
        latitude, longitude = (
            float(str(park["latitude"])),
            float(str(park["longitude"]))
            if "latitude" in park
            else util.get_coordinates(
                logging,
                maps_key,
                str(park["fullName"]) if phys_addr == "" else str(phys_addr),
            ),
        )
        logging.debug("      Got coords %.3f, %.3f", latitude, longitude)

        email = (
            park["contacts"]["emailAddresses"][0]["emailAddress"]
            if len(park["contacts"]["emailAddresses"]) > 0
            else ""
        )
        logging.debug("      Got email %s", email)

        new_park = Park(
            id=id_num,
            parkCode=park["parkCode"],
            name=str(park["fullName"]).title(),
            designation=park["designation"],
            max_fee=maximum_fee,
            activities_cnt=len(activity_list),
            states=park["states"],
            lat=latitude,
            long=longitude,
            image=image,
            imageCaption=image_caption,
            phys_address=phys_addr,
            mail_address=mail_addr,
            email=email,
            phone=phone_num,
            description=park["description"],
            fee_description=fee_description_dict,
            activities=activity_list,
        )
        parks_list.append(new_park)
        logging.debug("      Done with park %d", id_num)
        id_num = id_num + 1

    return parks_list


def get_lodging(
    logging: Logger, api_key: str, park_list: List[Park], radius: float, limit: int = 25
) -> List[Lodging]:
    """
    Get all lodging and their details from RIDB API using the Parks list.

    Return a list of Lodging instances filled out, linked to Parks (but not Trails)
    """
    assert api_key is not None
    session = requests.session()
    session.mount("https://", HTTPAdapter(max_retries=5))
    logging.info("Starting Lodging scrape:")
    id_num = 0
    # loop through each park and request all lodging close to each park
    request_url = "https://ridb.recreation.gov/api/v1/facilities"
    # list of lodges instances to be added to db
    lodges_list = []
    # set of lodge FacilityIDs to avoid duplicates
    lodges_set = set()
    for park in park_list:
        params = {
            "query": "Campground",
            "full": "true",
            "latitude": str(park.lat),
            "longitude": str(park.long),
            "radius": str(radius),
            "apikey": str(api_key),
            "per_page": str(limit),
        }
        r_quest = None
        try:
            logging.info("   Querying RIDB for park %s", park.name)
            r_quest = session.get(request_url, params=params)
            r_quest.raise_for_status()
            lodges = r_quest.json()

            logging.debug("   RECDATA length: %d", len(lodges["RECDATA"]))
            for item in lodges["RECDATA"]:
                fid = int(item["FacilityID"])
                if fid in lodges_set:
                    logging.warning(
                        "      Facility %s is already in the list", item["FacilityName"]
                    )
                    continue
                lodges_set.add(int(item["FacilityID"]))
                # scrape API for the correct data for the media
                image = ""
                image_caption = ""
                for media in item["MEDIA"]:
                    if media["MediaType"] == "Image":
                        image = media["URL"]
                        image_caption = media["Title"]
                        break
                logging.debug("      Image found: %s", str(image))

                address1, address2, state, location = "", "", "", ""
                if len(item["FACILITYADDRESS"]) > 0:
                    logging.debug(
                        "      FACILITYADDRESS length: %d", len(item["FACILITYADDRESS"])
                    )
                    address1 = item["FACILITYADDRESS"][0]["FacilityStreetAddress1"]
                    location = item["FACILITYADDRESS"][0]["City"]
                    state = item["FACILITYADDRESS"][0]["AddressStateCode"]
                    address2 = (
                        (
                            location
                            + ", "
                            + state
                            + " "
                            + item["FACILITYADDRESS"][0]["PostalCode"]
                        )
                        if len(location) > 0
                        else "Located in " + state
                        if len(state) > 0
                        else "No address available"
                    )
                    logging.debug("      Address2 string: %s", address2)
                else:
                    logging.warning("      No facility addresses found")

                activity_list = []
                logging.debug("      ACTIVITY length: %d", len(item["ACTIVITY"]))
                for activity in item["ACTIVITY"]:
                    activity_list.append(str(activity["ActivityName"]).title())
                fee_desc = str(item["FacilityUseFeeDescription"])
                fee_desc = fee_desc if len(fee_desc) > 0 else "No fees specified"
                new_lodge = Lodging(
                    id=id_num,
                    name=str(item["FacilityName"]).title(),
                    location=location,
                    state=state,
                    lodging_type=item["FacilityTypeDescription"],
                    image=image,
                    imageCaption=image_caption,
                    address1=address1,
                    address2=address2,
                    lat=float(item["FacilityLatitude"]),
                    long=float(item["FacilityLongitude"]),
                    email=item["FacilityEmail"],
                    phone=item["FacilityPhone"],
                    fee_description=fee_desc,
                    activities=activity_list,
                    reservable=bool(item["Reservable"]),
                    description=item["FacilityDescription"],
                )
                park.lodging.append(new_lodge)
                logging.debug("   Created %s", new_lodge.name)
                lodges_list.append(new_lodge)
                id_num = id_num + 1
        except RequestException as err:
            logging.exception(
                "   Got bad response from RIDB or failed to call: %s", err
            )
            continue
        except Exception as err:
            logging.exception("   Unexpected error: %s", err)
            continue
        else:
            logging.debug("   Got good response from RIDB")

    return lodges_list


def get_trails(
    logging: Logger, api_key: str, maps_key: str, park_list: List[Park], limit: int = 25
) -> List[Trail]:
    """
    Get all trails and their details from NPS API and Google Maps API (if needed)
    using the Parks list.

    Return a list of Trail instances filled out, linked to Parks (but not Lodging)
    """
    assert api_key is not None
    assert maps_key is not None
    logging.info("Starting trails scrape:")
    trails_list = []
    trails_set = set()
    session = requests.session()
    session.mount("https://", HTTPAdapter(max_retries=1))

    key_words = ["hike", "hiking", "bike", "biking", "trail"]

    for park in park_list:
        # get the park code to make looking for trails easier
        park_code = park.parkCode

        # now lookup things to do for the given park which includes information on hiking and trails
        request_url = "https://developer.nps.gov/api/v1/thingstodo"
        trails_query = {
            "parkCode": park_code,
            "q": "trail",
            "limit": limit,
            "api_key": str(api_key),
        }
        trails_response = None
        try:
            logging.info("   Querying NPS API for park %s", park.name)
            trails_response = session.get(request_url, params=trails_query)
            trails_response.raise_for_status()
            trails = trails_response.json()
            for trail in trails["data"]:
                trail_name = (
                    str(trail["location"])
                    if len(str(trail["location"])) > 0
                    else str(trail["title"])
                ).replace("/", " ")
                if (
                    (
                        "hike" in map(lambda x: str(x).lower(), trail["tags"])
                        or "hiking" in map(lambda x: str(x).lower(), trail["tags"])
                    )
                    and (any(word in trail["title"].lower() for word in key_words))
                ) and trail_name not in trails_set:
                    logging.debug("   Found potential trail %s", trail_name)
                    trails_set.add(trail_name)
                    # get states of related parks, store as comma-delimited string
                    states_list = ""
                    for related_park in trail["relatedParks"]:
                        states_list += related_park["states"] + ","
                    states_list = states_list[:-1]

                    # get latitude and longitude from google maps api if needed
                    latitude = park.lat
                    longitude = park.long
                    if trail["latitude"] == "" or trail["longitude"] == "":
                        # call the utility function for coordinates
                        logging.warning(
                            "   Could not find coordinates, calling Google Maps..."
                        )
                        coords = util.get_coordinates(
                            logging, maps_key, trail_name + " " + park.name
                        )
                        latitude = coords[0] if coords is not None else park.lat
                        longitude = coords[1] if coords is not None else park.long
                    else:
                        logging.debug("   Found coordinates for trail %s")
                        latitude = float(trail["latitude"])
                        longitude = float(trail["longitude"])

                    # extract trail type and difficulty
                    (
                        trail_difficulty,
                        trail_distance,
                        description,
                    ) = util.extract_description(logging, trail)

                    # build activities list
                    activities_list = []
                    for activity in trail["activities"]:
                        activities_list.append(activity["name"].lower())
                    logging.debug("   Got %d activities", len(activities_list))

                    # build topics list
                    topics_list = []
                    for topic in trail["topics"]:
                        topics_list.append(topic["name"].lower())
                    logging.debug("   Got %d topics", len(topics_list))

                    new_trail = Trail(
                        name=trail_name,
                        title=str(trail["title"]),
                        image=trail["images"][0]["url"],
                        imageCaption=trail["images"][0]["caption"],
                        states=states_list,
                        lat=latitude,
                        long=longitude,
                        difficulty=trail_difficulty,
                        distance=trail_distance,
                        pets=trail["arePetsPermitted"].lower() == "true",
                        fees=trail["doFeesApply"].lower() == "true",
                        reservation=trail["isReservationRequired"].lower() == "true",
                        activities=activities_list,
                        topics=topics_list,
                        tags=trail["tags"],
                        description=description,
                    )
                    park.trails.append(new_trail)
                    trails_list.append(new_trail)
                    logging.debug("   Created %s!", new_trail.name)
                else:
                    logging.warning("      Trail %s is already in the list", trail_name)
                    continue
        except RequestException as err:
            logging.exception(
                "   Got bad response from NPS API or failed to call: %s", err
            )
            continue
        except Exception as err:
            logging.exception("   Unexpected error: %s", err)
            continue
        else:
            logging.debug("   Got good response from NPS API")

    return trails_list


def link_lt(
    logging: Logger, lodging_list: List[Lodging], trail_list: List[Trail], radius: float
) -> None:
    """
    Link lodging and trail instances by proximity (within given radius, in miles)
    Modifies lodging_list and trail_list directly to link instances
    Sorts lodging_list in-place as well
    """

    # source for lat/long calculations:
    # https://gis.stackexchange.com/questions/142326/calculating-longitude-length-in-miles
    logging.info("Starting Trails/Lodging linking:")
    lodging_list.sort(key=lambda x: float(x.long))
    mi_per_lat = 69.0
    lat_r = radius / mi_per_lat
    logging.debug("   Range: %.1f miles", radius)
    logging.debug("   Latitude range: %.3f degrees", lat_r)
    for trail in trail_list:
        logging.debug(
            "   Now on trail %s, lat=%.3f, long=%.3f", trail.name, trail.lat, trail.long
        )
        mi_per_lng = abs(cos(trail.lat * pi / 180) * 69.172)
        lng_r = radius / mi_per_lng
        logging.debug("   Longitude range: %.3f degrees", lng_r)
        filtered = util.coord_filter(
            logging,
            lodging_list,
            (trail.long - lng_r, trail.long + lng_r),
            (trail.lat - lat_r, trail.lat + lat_r),
        )
        for lodge in filtered:
            trail.lodging.append(lodge)
    logging.info("   Trails/Lodging linking complete!")
